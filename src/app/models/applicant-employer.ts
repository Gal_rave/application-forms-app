export class ApplicantEmployer {
    public ID:number;
    public Employer:string;
    public Name:string;
    public Address:string;
    public Position:string;
    public ResignationCause:string;
    public FromDate:Date;
    public ToDate:Date;
}
