import { IncomeTypeModel } from './income-type-model';

export class EmployerModel {
    public ID: number;
    public name: string;
    public address: string;
    public phoneNumber: string;
    public deductionFileID: string;
    public email: string;
    public displayEmail: boolean;
    public emailEmployee: boolean;
    public emailEmployer: boolean;
    public salary: number;
    public taxDeduction: number;
    public mifal: number;
    public rashut: number;
    public Checkaddress: number;
    public CheckdeductionFileID: number;
    public CheckincomeType: number;
    public Checkmifal: number;
    public Checkname: number;
    public Checkrashut: number;
    public Checksalary: number;
    public ChecktaxDeduction: number;
    public incomeType: IncomeTypeModel;
}
