export class IncomeTypeModel {
    public monthSalary: boolean;
    public anotherSalary: boolean;
    public partialSalary: boolean;
    public daySalary: boolean;
    public allowance: boolean;
    public stipend: boolean;
    public anotherSource: boolean;
    public anotherSourceDetails: string;
    public Checkallowance: number;
    public CheckanotherSalary: number;
    public CheckanotherSource: number;
    public CheckanotherSourceDetails: number;
    public CheckdaySalary: number;
    public CheckmonthSalary: number;
    public CheckpartialSalary: number;
    public Checkstipend: number;
}
