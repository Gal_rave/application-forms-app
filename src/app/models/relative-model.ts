export class RelativeModel {
    public ID:number = null;
    public KinshipCode:number = null;
    public FirstName:string = null;
    public KinshipText:string = null;
    public LastName:string = null;
    public Unit:string = null;
    public idNumber:string = null;
}