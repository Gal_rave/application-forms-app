export class Pager {
    constructor(list: any[], pageSize?: number) {
        // if (!(list instanceof Array)) {
        //     list = [];
        // }
        this.PageNumber = 0;
        this.PageSize = pageSize ?? 20;
        this.Counter = list.length;
        this.MaxPages = Math.ceil(this.Counter / this.PageSize);
        this.PagerList = Array(this.MaxPages).fill(0).map((x, i) => (i));
    }
    public PageSize: number;
    public MaxPages: number;
    public PageNumber: number;
    public Counter: number;
    public PagerList: number[];
}
