export class MultiSelectTexts {
    public constructor(_checkAll?: string,
        _uncheckAll?: string,
        _checked?: string,
        _checkedPlural?: string,
        _searchPlaceholder?: string,
        _searchEmptyResult?: string,
        _searchNoRenderText?: string,
        _defaultTitle?: string,
        _allSelected?: string) {
        this.checkAll = _checkAll !== undefined ? _checkAll : 'בחר הכל';
        this.uncheckAll = _uncheckAll !== undefined ? _uncheckAll : 'בטל בחירה';
        this.checked = _checked !== undefined ? _checked : 'נבחר';
        this.checkedPlural = _checkedPlural !== undefined ? _checkedPlural : 'נבחרו';
        this.searchPlaceholder = _searchPlaceholder !== undefined ? _searchPlaceholder : 'חפש';
        this.searchEmptyResult = _searchEmptyResult !== undefined ? _searchEmptyResult : 'אין תוצאות';
        this.searchNoRenderText = _searchNoRenderText !== undefined ? _searchNoRenderText : 'הקלד בתיבת החיפוש...';
        this.defaultTitle = _defaultTitle !== undefined ? _defaultTitle : 'בחר...';
        this.allSelected = _allSelected !== undefined ? _allSelected : 'הכל נבחר';
    }
    public checkAll: string;
    public uncheckAll: string;
    public checked: string;
    public checkedPlural: string;
    public searchPlaceholder: string;
    public searchEmptyResult: string;
    public searchNoRenderText: string;
    public defaultTitle: string;
    public allSelected: string;
}
