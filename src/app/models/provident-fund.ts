export class ProvidentFund {
    public ID:number = null;
    public DefaultPos:number = null;
    public PosType:number = null;
    public TreasuryCode:number = null;
    public TreasuryNumber:string = null;
    public MustDefaultPos: number = null;
}
