export class ReferenceModel {
    public ID:number = null;
    public Recommender:string = null;
    public WorkPlace:string = null;
    public jobTitle:string = null;
    public Email:string = null;
    public Phone:string = null;
}
