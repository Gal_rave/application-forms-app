import { EmployerModel } from './employer-model';

export class TaxCoordinationModel {
    public Checkreason: number;
    public Checkrequest: number;
    public employers: Array<EmployerModel>;
    public requestCoordination: boolean;
    public requestReason: number;
}
