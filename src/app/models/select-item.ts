export class SelectItem {
    constructor(value: string, key: number) {
        this.Key = key;
        this.Value = value;
    }
    public Value: string;
    public Key: number;
}
