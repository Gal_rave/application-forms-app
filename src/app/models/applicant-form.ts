import { EmployeeModel } from './employee-model';
import { ChildModel } from './child-model';
import { SpouseModel } from './spouse-model';
import { ApplicantEmployer } from './applicant-employer';
import { Language } from './language';
import { Course } from './course';
import { Travel } from './travel';
import { TemplateModel } from './template-model';
import { TaxExemptionModel } from './tax-exemption-model';
import { EmployerModel } from './employer-model';
import { IncomeModel } from './income-model';
import { TaxCoordinationModel } from './tax-coordination-model';
import { IncomeTypeModel } from './income-type-model';
import { ProvidentFund } from './provident-fund';
import { ReferenceModel } from './reference-model';
import { RelativeModel } from './relative-model';
import { EmergencyContactModel } from './emergency-contact-model';

export class ApplicantForm {
    public showMobile: boolean = null;
    public year: number = null;
    public date: Date = null;
    public municipalityId: number = null;
    public version: string = null;
    public FormType: number = null;

    public taxExemption: TaxExemptionModel;
    public income: IncomeModel;
    public taxCoordination: TaxCoordinationModel;
    public incomeType: IncomeTypeModel;
    public otherIncomes: IncomeModel;
    public employee: EmployeeModel = new EmployeeModel();
    public employer: EmployerModel = new EmployerModel();
    public spouse: SpouseModel = new SpouseModel();
    public Travel: Travel = new Travel();

    public children: Array<ChildModel> = new Array<ChildModel>();
    public Experience: Array<ApplicantEmployer> = new Array<ApplicantEmployer>();
    public Languages: Array<Language> = new Array<Language>();
    public Courses: Array<Course> = new Array<Course>();
    public Templates: Array<TemplateModel> = new Array<TemplateModel>();
    public MunicipalityForms: Array<number> = new Array<number>();
    public AFImages: Array<number> = new Array<number>();
    public ProvidentFunds: Array<ProvidentFund> = new Array<ProvidentFund>();
    public References: Array<ReferenceModel> = new Array<ReferenceModel>();
    public Relatives: Array<RelativeModel> = new Array<RelativeModel>();
    public EmergencyContacts: Array<EmergencyContactModel> = new Array<EmergencyContactModel>();
}
