import { MultiSelectSet } from '../interfaces/multi-select-set';

export class MultiSelectSettings {
    public constructor(...args: MultiSelectSet[]) {
        args.forEach((arg: MultiSelectSet) => {
            this[arg.id] = arg.value;
        });
    }

    public pullRight: boolean = true;
    public enableSearch: boolean = false;
    public buttonClasses: string = 'form-control MultiSelect-button';
    public itemClasses: string = '';
    public containerClasses: string = 'MultiSelect-container';
    public selectionLimit: number = 0;
    public minSelectionLimit: number = 0;
    public autoUnselect: boolean = false;
    public closeOnSelect: boolean = false;
    public showCheckAll: boolean = false;
    public showUncheckAll: boolean = false;
    public fixedTitle: boolean = false;
    public dynamicTitleMaxItems: number = 0;
    public maxHeight: string = '250px';
    public displayAllSelectedText: boolean = true;
    public searchRenderLimit: number = 0;
    public searchRenderAfter: number = 1;
    public searchMaxLimit: number = 0;
    public searchMaxRenderedItems: number = 0;
    public closeOnClickOutside: boolean = true;
    public isLazyLoad: boolean = false;
    public loadViewDistance: number = 1;
    public stopScrollPropagation: boolean = false;
    public selectAddedValues: boolean = false;
    public ignoreLabels: boolean = false;
    public maintainSelectionOrderInTitle: boolean = false;
    public focusBack: boolean = true;
}
