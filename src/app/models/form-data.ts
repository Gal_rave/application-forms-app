import { ApplicantForm } from './applicant-form';

export class FormData {
    public ID: number = null;
    public JSONDATA: string = null;
    public USERID: number = null;
    public CREATEDDATE: Date = null;
    public VERSION: string = null;
    public DELIVERYSTATUS: number = null;
    public MUNICIPALITYID: number = null;
    public TYPE: number = null;
    public MANUALCONFIRMATION: number = null;
    public CONFIRMATION: number = null;
    public CONFIRMATIONDATE: Date = null;
    public COMMENTTEXT: string = null;
    public PERSONNELAPPROVAL: number = null;
    public PERSONNELAPPROVALDATE: Date = null;
    public UPDATINGUSER: number = null;
    public UPDATINGUSERDATE: Date = null;
    public UPDATEDATE: Date = null;
    public USERNAME: string = null;
    public PERSONNELAPPROVAL_USERNAME:string = null;
    public UPDATINGUSER_USERNAME:string = null;
    public IFN_TRANSACTIONID: string = null;
    public MIFAL: number = null;
    public SEIF_TAKZIVI_RASHI: number = null;
    public NIM_SHEM_MISHTAMESH: string = null;
    public NIM_EMAIL: string = null;
    
    public ERU_KAV_KOD_MEDAVEACH: number = null;
    public ERU_KAV_ASMACHTA:number = null;
    public ERU_OVD_KTV_EMAIL:string = null;
    
    public AppData: ApplicantForm = new ApplicantForm();
    public formsCounter: number = null;
    public IsReturned:boolean = null;
    public touched: boolean = false;
}