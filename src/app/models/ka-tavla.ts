export class KaTavla {
    public PNH_TEUR_K:string = null;
    public PNH_EREH_K_NUMERI:number = null;
    public PNH_EREH_A :string = null;
    public PNH_EREH_K :number = null;
    public PNH_MISP_TAVLA: number = null;
}
