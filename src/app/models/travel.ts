import { TransportModes } from './transport-modes';

export class Travel {
    public Declaration:boolean;
    public DeclarationChanges:boolean;
    public TransportModes:Array<TransportModes>;
}
