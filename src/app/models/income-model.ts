import { IncomeTypeModel } from './income-type-model';

export class IncomeModel {
    public CheckhasIncomes: number;
    public CheckincomeTaxCredits: number;
    public ChecktrainingFund: number;
    public CheckworkDisability: number;
    public CheckanotherSourceDetails: number;
    public CheckstartDate: number;
    public incomeType: IncomeTypeModel;
    public startDate: Date;
    public incomeTaxCredits: number;
    public trainingFund: boolean;
    public workDisability: boolean;
    public anotherSourceDetails: string;
    public hasIncomes: boolean;
}
