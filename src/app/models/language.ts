export class Language {
    public ID:number;
    public name:string;
    public reading:boolean;
    public readingLevel:number;
    public speech:boolean;
    public speechLevel:number;
    public writing:boolean;
    public writingLevel:number;
}
