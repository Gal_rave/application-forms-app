import { ApplicantForm } from './applicant-form';

export class ShowForms {
    constructor(forms: number[], userData: ApplicantForm) {
        /*פרטים אישיים*/
        if (forms.indexOf(1) >= 0) {
            this.AllowPersonalInformation = true;
        }
        /*החזר נסיעות*/
        if (forms.indexOf(2) >= 0 && userData !== null && userData.Travel.TransportModes.length > 0) {
            this.AllowTravelReimbursement = true;
        }
        /*הצטרפות להסתדרות*/
        if (forms.indexOf(3) >= 0 && userData !== null && (userData.employee.organizationalAffiliation === 2 || userData.employee.organizationalAffiliation === 3)) {
            this.AllowWorkersUnion = true;
        }
        /*קופת גמל*/
        if (forms.indexOf(4) >= 0 && userData !== null && userData.ProvidentFunds.length > 0) {
            this.AllowProvidentFund = true;
        }
        /*טופס 101*/
        if (forms.indexOf(669) >= 0) {
            this.AllowTaxForm = true;
        }
        /*תצוגה של דפי הסכמה (דפי סימון V)*/
        if (forms.indexOf(5) >= 0) {
            this.AllowCustomPdf = true;
        }
        /*קרובים בארגון*/
        if (forms.indexOf(6) >= 0) {
            this.AllowRelativesForm = true;
        }
        /*ממליצים*/
        if (forms.indexOf(7) >= 0) {
            this.AllowReferencesForm = true;
        }
        /*קרובים לשעת חרום*/
        if (forms.indexOf(8) >= 0) {
            this.AllowEmergencyContacts = true;
        }
    }

    public PersonalInformation: boolean = false;
    public AllowPersonalInformation: boolean = false;

    public ReferencesForm: boolean = false;
    public AllowReferencesForm: boolean = false;

    public RelativesForm: boolean = false;
    public AllowRelativesForm: boolean = false;

    public EmergencyContactsForm: boolean = false;
    public AllowEmergencyContacts: boolean = false;

    public TravelReimbursement: boolean = false;
    public AllowTravelReimbursement: boolean = false;

    public WorkersUnion: boolean = false;
    public AllowWorkersUnion: boolean = false;

    public TaxForm: boolean = false;
    public AllowTaxForm: boolean = false;

    public ProvidentFund: boolean = false;
    public AllowProvidentFund: boolean = false;

    public AllowCustomPdf: boolean = false;
}
