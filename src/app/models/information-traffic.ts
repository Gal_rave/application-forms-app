import { InformationTrafficResponse } from './information-traffic-response';

export class InformationTraffic {
    public UserID: string;
    public MunicipalityId: number;
    public DocumentUserId: string;
    public DocumentUserName: string;
    public FormID: number;
    public ImageTypes: Array<number>;
    public EntryDate: Date;
    public Response: InformationTrafficResponse;
}
