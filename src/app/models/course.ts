export class Course {
    public ID:number;
    public certificateName:string;
    public degree:string;
    public degreeDate:Date;
    public educationYears:number;
    public institutionName:string;
    public studySubject1:string;
    public studySubject2:string;
}
