export class TransportModes {
    public ID:number;
    public Days:number;
    public FromStation:string;
    public ToStation:string;
    public LineNumber:string;
    public ModeType:string;
    public MonthlyTicket:number;
    public Price:number;
}
