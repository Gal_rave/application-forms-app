export class EmergencyContactModel {
    public ID: number = null;
    public FirstName: string = null;
    public LastName: string = null;
    public idNumber: string = null;
    public cell: string = null;
    public city: string = null;
    public cityID: number = null;
}