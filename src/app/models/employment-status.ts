export class EmploymentStatus {
    public CODE: number = null;
    public STATUS: number = null;
    public PENSIONSTATUS: number = null;
    public DESCRIPTION: string;
}
