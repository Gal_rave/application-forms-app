export class InformationTrafficResponse {
    public Error: any;
    public Message: string = null;
    public ResponseCode: number = null;
    public TransactionID: string = null;
}
