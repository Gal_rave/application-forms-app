import { HttpClient } from '@angular/common/http';
import { catchError, tap, retry } from 'rxjs/internal/operators';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ErrorHandlerService } from './error-handler.service';
import { UrlMapService } from './url-map.service';
import { FactoryModel } from '../models/factory-model';
import { Authorization } from '../models/authorization';
import { AuthorizationService } from './authorization.service';
import { BudgetItem } from '../models/budget-item';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { ParentMassageService } from './parent-massage.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private baseUrl = 'api/Form101/';
  private currentUser: Authorization;

  public FactoriesOptions: BehaviorSubject<IMultiSelectOption[]>;
  public BudgetItemsOptions: BehaviorSubject<IMultiSelectOption[]>;
  public RashutFactorys: Observable<FactoryModel[]>;
  public BudgetItems: Observable<BudgetItem[]>;

  constructor(
    private http: HttpClient,
    private ErrorHandler: ErrorHandlerService,
    private urlMap: UrlMapService,
    private authenticationService: AuthorizationService,
    private parentMassage: ParentMassageService) {
    this.currentUser = this.authenticationService.currentUserValue;

    if (sessionStorage.getItem('RashutFactorys') !== null) {
      const data: BehaviorSubject<FactoryModel[]> = new BehaviorSubject<FactoryModel[]>(JSON.parse(sessionStorage.getItem('RashutFactorys')));
      this.RashutFactorys = data.asObservable();
      this.FactoriesOptions = new BehaviorSubject<IMultiSelectOption[]>(
        data.value.map((Factory: FactoryModel) => {
          return { id: Factory.code, name: Factory.name };
        })
      );
    }
    if (sessionStorage.getItem('BudgetItems') !== null) {
      const data: BehaviorSubject<BudgetItem[]> = new BehaviorSubject<BudgetItem[]>(JSON.parse(sessionStorage.getItem('BudgetItems')));
      this.BudgetItems = data.asObservable();
      this.BudgetItemsOptions = new BehaviorSubject<IMultiSelectOption[]>(
        data.value.map((item: BudgetItem) => {
          return { id: item.code, name: `${item.description} (${item.code})` };
        })
      );
    }
  }

  private get getRashutFactorys(): Observable<FactoryModel[]> {
    return this.RashutFactorys;
  }
  private get getBudgetItems(): Observable<BudgetItem[]> {
    return this.BudgetItems;
  }

  public GetRashutFactorys(): Observable<FactoryModel[]> {
    if (this.RashutFactorys instanceof Observable) {
      return this.getRashutFactorys;
    }

    return this.http.put<FactoryModel[]>(this.urlMap.SetUrl(this.baseUrl + 'RashutFactorys/[Rashut]', 3, { Rashut: this.currentUser.MunicipalityId }), null, this.ErrorHandler.httpOptions)
      .pipe(
        tap((factorys: FactoryModel[]) => {
          sessionStorage.setItem('RashutFactorys', JSON.stringify(factorys));
          this.FactoriesOptions = new BehaviorSubject<IMultiSelectOption[]>(
            factorys.map((Factory: FactoryModel) => {
              return { id: Factory.code, name: Factory.name };
            })
          );
        })
      );
  }
  public GetBudgetItems(): Observable<BudgetItem[]> {
    if (this.BudgetItems instanceof Observable) {
      return this.getBudgetItems;
    }

    return this.http.put<BudgetItem[]>(this.urlMap.SetUrl(this.baseUrl + 'BudgetItems/[Rashut]', 3, { Rashut: this.currentUser.MunicipalityId }), null, this.ErrorHandler.httpOptions)
      .pipe(
        tap((budgetItems: BudgetItem[]) => {
          sessionStorage.setItem('BudgetItems', JSON.stringify(budgetItems));
          this.BudgetItemsOptions = new BehaviorSubject<IMultiSelectOption[]>(
            budgetItems.map((item: BudgetItem) => {
              return { id: item.code, name: `(${item.code}) ${item.description}` };
            })
          );
        })
      );
  }
  public GetFactory(fuctory: number): IMultiSelectOption {
    return this.FactoriesOptions.value.find(f => f.id === fuctory);
  }
  public GetBudgetItem(budgetItem: number): IMultiSelectOption {
    return this.BudgetItemsOptions.value.find(f => f.id === budgetItem);
  }

  public get CurrentFactoriesOptions(): Observable<IMultiSelectOption[]> {
    return this.FactoriesOptions.asObservable();
  }
  public get CurrentBudgetItemsOptions(): Observable<IMultiSelectOption[]> {
    return this.BudgetItemsOptions.asObservable();
  }
}
