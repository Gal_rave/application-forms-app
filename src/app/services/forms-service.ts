
import { HttpClient } from '@angular/common/http';
import { catchError, tap, retry } from 'rxjs/internal/operators';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import * as _ from 'jquery';

import { environment } from '../../environments/environment';
/*services*/
import { ErrorHandlerService } from './error-handler.service';
import { Authorization } from '../models/authorization';
import { UrlMapService } from './url-map.service';
import { AuthorizationService } from './authorization.service';

/*classes*/
import { FormData } from '../models/form-data';
import { RecursiveUpdateService } from './recursive-update.service';
import { KaTavla } from '../models/ka-tavla';
import { InformationTraffic } from '../models/information-traffic';
import { Form101Data } from '../interfaces/form101-data';

@Injectable({
  providedIn: 'root'
})
export class FormsService {
  private baseUrl = 'api/Application/';
  private currentUser: Authorization;
  private currentFormsSubject: BehaviorSubject<Array<FormData>>;
  public currentForms: Observable<Array<FormData>>;

  constructor(
    private http: HttpClient,
    private ErrorHandler: ErrorHandlerService,
    private urlMap: UrlMapService,
    private authenticationService: AuthorizationService) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  public GetApplicants(): Observable<FormData[]> {
    if (this.getCurrentForms instanceof Observable) {
      this.CurrentFormsValue.forEach(frm => {
        frm = RecursiveUpdateService.recursiveUpdate(frm);
      });
      return this.getCurrentForms;
    }

    return this.http.post<FormData[]>(this.urlMap.SetUrl(this.baseUrl + 'GetApplicants/[rashot]', 3, { rashot: this.currentUser.MunicipalityId }), null, this.ErrorHandler.httpOptions)
      .pipe(
        tap((forms: Array<FormData>) => {
          forms.map((form: FormData) => {
            /*import data run time set up*/
            form = RecursiveUpdateService.recursiveUpdate(form);
          });
          this.SetApplicationForms(forms);
        })
      );
  }

  public GetBase(table: number): Observable<KaTavla[]> {
    return this.http.put<KaTavla[]>(this.urlMap.SetUrl(this.baseUrl + 'BaseData/[tableNum]', 3, { tableNum: table }), null, this.ErrorHandler.httpOptions)
      .pipe(
        tap((data: Array<KaTavla>) => {
          data.map(d => d = RecursiveUpdateService.Clean(d, KaTavla));
        })
      );
  }

  public UpDateForms(forms: FormData[]): Observable<FormData[]> {
    return this.http.post<FormData[]>(this.urlMap.SetUrl(this.baseUrl + 'UpdateForms', 2), forms.map(form => this.reSetData(form)), this.ErrorHandler.httpOptions)
      .pipe(
        tap((returndForms: FormData[]) => {
          returndForms.map(returndForm => {
            this.CurrentFormsValue[this.CurrentFormsValue.indexOf(this.CurrentFormsValue.find(frm => frm.USERID === returndForm.USERID && frm.MUNICIPALITYID ===
              returndForm.MUNICIPALITYID))] = RecursiveUpdateService.recursiveUpdate(returndForm);
          });
        })
      );
  }

  public Update(form: FormData): Observable<FormData> {
    form.JSONDATA = JSON.stringify(form.AppData);
    return this.http.put(this.urlMap.SetUrl(this.baseUrl + 'UpdateForm', 2), this.reSetData(form), this.ErrorHandler.httpOptions)
      .pipe(
        tap((returndForm: FormData) => {
          this.CurrentFormsValue[this.CurrentFormsValue.indexOf(this.CurrentFormsValue.find(frm => frm.ID === returndForm.ID))] = RecursiveUpdateService.recursiveUpdate(returndForm);
        })
      );
  }

  public ReImportUserData(form: FormData): Observable<Form101Data>  {
    return this.http.put<Form101Data>(this.urlMap.SetUrl(this.baseUrl + 'ReSetApplicationForm', 2), { idNumber: form.USERID, municipalityId: form.MUNICIPALITYID }, this.ErrorHandler.httpOptions)
      .pipe(
        tap((action: Form101Data) => {
          this.CurrentFormsValue[this.CurrentFormsValue.indexOf(this.CurrentFormsValue.find(frm => frm.ID === form.ID))] = RecursiveUpdateService.recursiveUpdate(form);
        })
      );
  }

  public SetApplicationForms(forms: Array<FormData>) {
    this.currentFormsSubject = new BehaviorSubject<Array<FormData>>(forms);
    this.currentForms = this.currentFormsSubject.asObservable();
  }

  public SaveDocumentsToIfn(information: InformationTraffic): Observable<InformationTraffic> {
    information.UserID = this.currentUser.idNUmber;
    return this.http.post<InformationTraffic>(this.urlMap.SetUrl(this.baseUrl + 'SaveDocuments', 2), information, this.ErrorHandler.httpOptions);
  }

  public SetUplicationForm(form: FormData): void {
    form.JSONDATA = JSON.stringify(form.AppData);
    this.CurrentFormsValue[this.CurrentFormsValue.indexOf(this.CurrentFormsValue.find(frm => frm.ID === form.ID))] = RecursiveUpdateService.recursiveUpdate(form);
  }

  public get CurrentFormsValue(): Array<FormData> {
    return this.currentFormsSubject.value;
  }

  public ClearFormsValue(): void {
    sessionStorage.removeItem('currentForms');
    if (this.currentFormsSubject !== undefined) { this.currentFormsSubject.next(null); }
    if (this.currentForms !== undefined) { this.currentForms = null; }
  }

  private get getCurrentForms(): Observable<Array<FormData>> {
    return this.currentForms;
  }

  private reSetData(form: FormData): FormData {
    form.CONFIRMATION = form.MANUALCONFIRMATION ? 1 : 0;
    form.MANUALCONFIRMATION = form.CONFIRMATION;
    return form;
  }
}
