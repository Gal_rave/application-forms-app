import { Injectable } from '@angular/core';

import { FormData } from '../models/form-data';

@Injectable({
  providedIn: 'root'
})
export class RecursiveUpdateService {
  static recursiveUpdate(form: any): FormData {
    /*aplication data*/
    form.AppData = JSON.parse(form.JSONDATA);
    /*custom parameters*/
    form.formsCounter = 0;

    if (form.JSONDATA !== null && form.AppData !== null) {
      RecursiveUpdateService.countForms(form);
    } else if (form.PERSONNELAPPROVAL === undefined || form.PERSONNELAPPROVAL === null) {
      form.PERSONNELAPPROVAL = 0;
    }
    if (!form.touched){
      RecursiveUpdateService.remove(form);
    }
    return RecursiveUpdateService.check(form);
  }

  static Clean(object: any, Type: any): any {
    const Typekeys = Object.keys(new Type());
    const DataKeys = Object.keys(object);

    DataKeys.forEach(key => {
      if (!Typekeys.includes(key)) {
        delete object[key];
      }
    });

    return object;
  }

  static remove(form: any): FormData {
    if (form.USERNAME === null && typeof form.SHM_PRATI === 'string' && typeof form.SHM_MISHPAHA === 'string'){
      form.USERNAME = `${form.SHM_PRATI} ${form.SHM_MISHPAHA}`;
    }
    if (form.USERID === null){
      form.USERID = form.MISPAR_MEZAE;
    }
    if (form.MUNICIPALITYID === null){
      form.MUNICIPALITYID = form.MISPAR_RASHUT;
    }
    const FormDataKeys = Object.keys(new FormData());
    const formKeys = Object.keys(form);
    if (FormDataKeys.length === formKeys.length) {
      form.touched = true;
      return form;
    }
    formKeys.forEach(key => {
      if (!FormDataKeys.includes(key)) {
        delete form[key];
      }
    });
    form.touched = true;
    return form;
  }

  static check(obj: any): any {
    for (var property in obj) {
      if (obj.hasOwnProperty(property)) {

        if (Array.isArray(obj[property])) {
          obj[property].map(RecursiveUpdateService.check);
        }
        else if (typeof obj[property] === "object" && obj[property] !== null) {
          RecursiveUpdateService.check(obj[property]);
        }
        else if ((property.toLowerCase().includes('date') || property.toLowerCase().includes('time'))
          && obj[property] !== null && typeof obj[property] === 'string') {
          obj[property] = new Date(obj[property]) < (new Date(1900, 0, 1)) ? new Date() : new Date(obj[property]);
        }
      }
    }

    return obj;
  }

  static countForms(form: FormData): FormData {
    if (!form.AppData.MunicipalityForms || form.AppData.MunicipalityForms.length <= 0)
      return form;
    /*פרטים אישיים*/
    if(form.AppData.MunicipalityForms.indexOf(1) > -1){
      form.formsCounter += 1;
    }
    /*קרובים בארגון OR ממליצים*/
    else if(form.AppData.MunicipalityForms.indexOf(6) > -1 || form.AppData.MunicipalityForms.indexOf(7) > -1){
      form.formsCounter += 1;
    }

    /*החזר נסיעות*/
    form.formsCounter += form.AppData.MunicipalityForms.indexOf(2) > -1 &&
      (form.AppData.Travel !== null && form.AppData.Travel.TransportModes !== null && form.AppData.Travel.TransportModes.length > 0) ? 1 : 0;
    /*הצטרפות להסתדרות*/
    form.formsCounter += form.AppData.MunicipalityForms.indexOf(3) > -1 &&
      (form.AppData.employee.organizationalAffiliation === 2 || form.AppData.employee.organizationalAffiliation === 3) ? 1 : 0;
    /*קופת גמל*/
    if (form.AppData.MunicipalityForms.indexOf(4) > -1) {
      if (form.AppData.ProvidentFunds === undefined)
        form.AppData.ProvidentFunds = [];
      form.formsCounter += form.AppData.MunicipalityForms.indexOf(4) > -1 && form.AppData.ProvidentFunds.length > 0 ? 1 : 0;
    }
    /*טופס 101*/
    form.formsCounter += form.AppData.MunicipalityForms.indexOf(669) > -1 ? 1 : 0;
    return form;
  }

}