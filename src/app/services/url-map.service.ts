import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlMapService {

  private baseUrl = 'api/Application/';

  constructor() { }

  public SetUrl(action:string, actionType:number = 1, parameters?: object):string{
    if(parameters !== null && parameters !== undefined){
      switch(actionType){
        case 1:///get
        action += '?';
        for(var i in parameters)
          action += i + '=' + String(parameters[i]) + '&';
        action = action.slice(0, action.lastIndexOf('&'));
        break;
        case 2:///post
        break;
        case 3:///rest
        for (var i in parameters)
          action = action.replace('[' + i + ']', (typeof parameters[i] !== 'undefined' && parameters[i] !== null ? parameters[i] : ''));
          action = action.replace(/\/\[([^)]+)\]/ig, '');
        break;
      }      
    }
    
    return action;
  }
  
}
