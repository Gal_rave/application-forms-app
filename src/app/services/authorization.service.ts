import { HttpClient } from '@angular/common/http';
import { catchError, retry } from 'rxjs/internal/operators';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

/*classes*/
import { Authorization } from '../models/authorization';
import { UrlMapService } from './url-map.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  private currentUserSubject: BehaviorSubject<Authorization>;
  public currentUser: Observable<Authorization>;

  private baseUrl = 'api/Application/';
  constructor(private http: HttpClient, private ErrorHandler: ErrorHandlerService, private urlMap: UrlMapService) {
    if (sessionStorage.getItem('currentUser') !== null) {
      this.currentUserSubject = new BehaviorSubject<Authorization>(JSON.parse(sessionStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
    }

  }

  public get currentUserValue(): Authorization {
    return this.currentUserSubject.value;
  }

  GetAuthorization(): Observable<Authorization> {
    return this.http.post<Authorization>(this.urlMap.SetUrl(this.baseUrl + 'GetAuthorization', 1), Authorization, this.ErrorHandler.httpOptions)
      .pipe(
        map(user => {
          sessionStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject = new BehaviorSubject<Authorization>(JSON.parse(sessionStorage.getItem('currentUser')));
          this.currentUser = this.currentUserSubject.asObservable();
          return user;
        })
      );
  }

  logout() {
    // remove user from local storage to log user out
    sessionStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

}
