import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export function getScrollHeight(): number {
  const content = $('body>app-root>div.content');
  let height = Math.max(content.height(), content.outerHeight()) + 100;
  return (window.self !== window.top && height < 750) ? 750 : height;
}

export function ReLoadIframe(Index: number): void {
  if (typeof Index === 'undefined' || Index > 5) {
    (<any>window).parent.postMessage(JSON.stringify({ msg: 'FullAbort', functionCall: 'clearInterval' }), "*");
    return void [0];
  }
  (<any>window).parent.postMessage(JSON.stringify({ msg: 2, functionCall: 'ReLoadIframe', scrollHeight: getScrollHeight() }), "*");
}

export function StartPage(): void {
  (<any>window).parent.postMessage(JSON.stringify({ msg: '\u05d8\u05d5\u05e4\u05e1\u05d9 \u05e7\u05dc\u05d9\u05d8\u05ea \u05e2\u05d5\u05d1\u05d3', functionCall: 'InsertModalDialog' }), "*");
  (<any>window).parent.postMessage(JSON.stringify({ functionCall: 'insertBlocker' }), "*");
  (<any>window).parent.postMessage(JSON.stringify({ msg: 1, functionCall: 'ReLoadIframe', scrollHeight: getScrollHeight() }), "*");
  (<any>window).parent.postMessage(JSON.stringify({ msg: 1, functionCall: 'insertSaveBtn' }), "*");
  (<any>window).parent.postMessage(JSON.stringify({ msg: 1, functionCall: 'insertExcelBtn' }), "*");

  setInterval(() => {
    (<any>window).parent.postMessage(JSON.stringify({ msg: 2, functionCall: 'ReLoadIframe', scrollHeight: getScrollHeight() }), "*");
  }, 1500);
}

@Injectable({
  providedIn: 'root'
})
export class ParentMassageService {
  public chatMessageAdded = new Subject();
  private isBlocking: boolean = false;
  private blockingCounter: number = 0;

  constructor() {
    let eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    let messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
    (<any>window).addEventListener(messageEvent, this.Eventer.bind(this));

    this.IframeReady(0);
  }

  public Eventer(event: any): void {
    const key = event.message ? 'message' : 'data';
    let data = event[key];
    if (key === 'data') {
      try {
        data = JSON.parse(event[key]);
      } catch (e) {/*console.error(e);*/ }
    }
    switch (data) {
      case 'externalExcelButton':
      case 'ExternalExcelClick':

      case 'externalSaveButton':
      case 'ExternalPFlikeClick':
        setTimeout(() => {
          this.chatMessageAdded.next(data);
        }, 25);
        break;
    }
  }

  public AddChatMessage(chatMessage: string): void {
    this.chatMessageAdded.next(chatMessage);
  }

  public toggleBlocking(block: boolean): void {
    if (block) {
      this.blockingCounter++;
      if (this.isBlocking)
        return;

      this.isBlocking = true;
      this.blockUI(true);
    } else {
      this.blockingCounter--;
      if (this.blockingCounter > 0)
        return;

      this.isBlocking = false;
      this.blockUI(false);
    }
    ReLoadIframe(0);
  }

  public CallReLoadIframe(Index: number): void {
    ReLoadIframe(Index);
  }

  public OpenPdfPopup(url: string, rashot: number, formId: number, formType: number): void {
    (<any>window).parent.postMessage(JSON.stringify({ msg: 0, functionCall: 'PopUp', url: url, rashot: rashot, formId: formId, imageType: formType }), "*");
  }

  public ParentMessage(pMsg: string): void {
    (<any>window).parent.postMessage(JSON.stringify({ msg: pMsg, functionCall: 'PopMessge' }), "*");
  }

  public IframeReady(Index: number) {
    if (Index > 5) {
      return void [0];
    }

    if (window.self !== window.top) {
      /* if has iframe start the iframe messeges*/
      window.addEventListener('load', function () {
        // StartPage();
        // ReLoadIframe(0);
        setTimeout(() => {
          StartPage();
          ReLoadIframe(0);
        }, (75 * Index));

      });
      /* if has iframe setup the mmesege services acordingly */
      setTimeout(() => {
        this.chatMessageAdded.next('frameElement');
      }, 1000);
    } else {
      setTimeout(() => {
        this.IframeReady(++Index);
      }, (75 * Index));
    }
  }

  public blockUI(start: boolean): void {
    (<any>window).parent.postMessage(JSON.stringify({ msg: (start ? 1 : 0), functionCall: 'blockUI' }), "*");
  }

}
