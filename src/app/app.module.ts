import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './interceptors/http-error-interceptor';
import { CookieService } from 'ngx-cookie-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { AppRoutingModule } from './app-routing.module';
import { CustomMaterialModule } from './custom-material/custom-material.module';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

/*directives*/
import { ShowDirective } from './directives/show.directive';
import { FadeElementDirective } from './directives/fade-element.directive';
import { OpenAllDirective } from './directives/open-all.directive';

/*pipes*/
import { DeliveryStatusPipe } from './pipes/delivery-status.pipe';
import { FullNamePipe } from './pipes/full-name.pipe';
import { EnumToArrayPipe } from './pipes/enum-to-array.pipe';
import { FilterEmployersPipe } from './pipes/filter-employers.pipe';

/*services*/
import { FormsService } from './services/forms-service';
import { AuthorizationService } from './services/authorization.service';

/*components*/
import { AppComponent } from './app.component';
import { FooterComponent } from './core/footer/footer.component';
import { HeaderComponent } from './core/header/header.component';
import { MainIndexComponent } from './core/pages/main-index/main-index.component';
import { PageNotFoundComponent } from './core/pages/page-not-found/page-not-found.component';
import { ShowFormComponent } from './core/pages/show-form/show-form.component';
import { PersonalInformationComponent } from './core/pages/personal-information/personal-information.component';
import { TravelReimbursementComponent } from './core/pages/travel-reimbursement/travel-reimbursement.component';
import { WorkersUnionComponent } from './core/pages/workers-union/workers-union.component';
import { TaxFormComponent } from './core/pages/tax-form/tax-form.component';
import { ChildrenComponent } from './core/pages/LIstsComponents/children/children.component';
import { LanguagesComponent } from './core/pages/LIstsComponents/languages/languages.component';
import { CoursesComponent } from './core/pages/LIstsComponents/courses/courses.component';
import { FamilyDetailsComponent } from './core/pages/LIstsComponents/family-details/family-details.component';
import { ExperienceComponent } from './core/pages/LIstsComponents/experience/experience.component';
import { GeneralInformationComponent } from './core/pages/LIstsComponents/general-information/general-information.component';
import { EmployeeDetailsComponent } from './core/pages/InnerTaxForm/employee-details/employee-details.component';
import { TaxFamilyDetailsComponent } from './core/pages/InnerTaxForm/tax-family-details/tax-family-details.component';
import { IncomeComponent } from './core/pages/InnerTaxForm/income/income.component';
import { IncomeTaxRebateComponent } from './core/pages/InnerTaxForm/income-tax-rebate/income-tax-rebate.component';
import { TaxCoordinationComponent } from './core/pages/InnerTaxForm/tax-coordination/tax-coordination.component';
import { TaxCoordinationEmployerComponent } from './core/pages/InnerTaxForm/tax-coordination-employer/tax-coordination-employer.component';
import { LengthFilterPipe } from './pipes/length-filter.pipe';
import { ProvidentFundComponent } from './core/pages/provident-fund/provident-fund.component';
import { ChildrenFilterPipe } from './pipes/children-filter.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RelativesComponent } from './core/pages/ListsComponents/relatives/relatives.component';
import { ReferencesComponent } from './core/pages/ListsComponents/references/references.component';
import { ReferencesAndRelativesComponent } from './core/pages/references-and-relatives/references-and-relatives.component';
import { EmergencyContactsComponent } from './core/pages/LIstsComponents/emergency-contacts/emergency-contacts.component';

@NgModule({
  declarations: [
    AppComponent,
    MainIndexComponent,
    PageNotFoundComponent,
    FooterComponent,
    HeaderComponent,
    ShowFormComponent,
    PersonalInformationComponent,
    FullNamePipe,
    DeliveryStatusPipe,
    ShowDirective,
    TravelReimbursementComponent,
    WorkersUnionComponent,
    TaxFormComponent,
    EnumToArrayPipe,
    ChildrenComponent,
    LanguagesComponent,
    CoursesComponent,
    FadeElementDirective,
    FamilyDetailsComponent,
    ExperienceComponent,
    GeneralInformationComponent,
    EmployeeDetailsComponent,
    TaxFamilyDetailsComponent,
    IncomeComponent,
    IncomeTaxRebateComponent,
    TaxCoordinationComponent,
    TaxCoordinationEmployerComponent,
    FilterEmployersPipe,
    OpenAllDirective,
    LengthFilterPipe,
    ProvidentFundComponent,
    ChildrenFilterPipe,
    RelativesComponent,
    ReferencesComponent,
    ReferencesAndRelativesComponent,
    EmergencyContactsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MultiselectDropdownModule,
    CustomMaterialModule,
    BsDatepickerModule.forRoot(),
    NgbModule
  ],
  providers: [
    CookieService,
    FormsService,
    AuthorizationService,
    EnumToArrayPipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
