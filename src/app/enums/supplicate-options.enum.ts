export enum SupplicateOptions {
    שכירZעצמאי = 1,
    גמלאיZית = 2,
    סטודנטZית = 3,
    מובטלZת = 4,
    קיבוץZמושב = 5,
    עובדZת_משק_בית = 6,
    התארגנות_ראשונית = 7
}
