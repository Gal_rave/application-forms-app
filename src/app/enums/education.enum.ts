export enum Education {
    B$A$ = 1,
    M$A$ = 2,
    BS$C$ = 3,
    LL$B$ = 4
}
