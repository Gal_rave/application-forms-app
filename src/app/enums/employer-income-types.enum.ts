export enum EmployerIncomeTypes {
    עבודה = 1,
    קיצבה = 2,
    מילגה = 3,
    אחר = 4
}
