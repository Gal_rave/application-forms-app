import { Component } from '@angular/core';
import { Router } from '@angular/router';

/*service*/
import { AuthorizationService } from './services/authorization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private Authorization:AuthorizationService, private router: Router) {
  }

  ngOnInit(): void {
    this.Authorization.GetAuthorization().subscribe(user=> {
      if(user !== null){
        this.router.navigate(['/Main']);
      }
    });
  }
}