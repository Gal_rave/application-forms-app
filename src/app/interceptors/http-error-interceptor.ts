import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { catchError, finalize } from 'rxjs/internal/operators';

import { AuthorizationService } from '../services/authorization.service';
import { ParentMassageService } from '../services/parent-massage.service';
import { SnackBarCaller } from '../custom-material/snack-bar-caller';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  private authenticationService = null;
  private postMassage = null;
  private barCaller = null;
  
  constructor(private injector: Injector) {
    this.authenticationService = this.injector.get(AuthorizationService);
    this.postMassage = this.injector.get(ParentMassageService);
    this.barCaller = this.injector.get(SnackBarCaller);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.postMassage.toggleBlocking(true);
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          this.postMassage.toggleBlocking(false);
          if ([401, 403].indexOf(error.status) !== -1) {
            this.barCaller.PopMessage(false, 'Unauthorized.', String(error.status));
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.authenticationService.logout();
            location.reload(true);
          }
          var ShowAction = false, _text = 'אנא תקן את הנתונים בהתאם.', _title = '';
          switch (error.status) {
            case 570://NewEmployeeDataException
              _text = 'תקלה כללית בשמירת הנתונים!';
              ShowAction = true;
              break;
            case 571://ArmyException
              _title = 'תקלה בשמירת פרטי צבא,';
              ShowAction = true;
              break;
            case 572://PartnerException
              _title = 'תקלה בשמירת פרטי בן זוג,';
              ShowAction = true;
              break;
            case 573://ChildrensException
              _title = 'תקלה בשמירת פרטי ילדים,';
              ShowAction = true;
              break;
            case 574://OrganizationalAffiliationException
              _title = 'תקלה בשמירת השתייכות ארגונית, ';
              ShowAction = true;
              break;
            case 575://PersonalInformationException
              _title = 'תקלה בשמירת פרטים אישיים,';
              ShowAction = true;
              break;
            case 576://MaritalStatusException
              _title = 'תקלה בשמירת מצב משפחתי,';
              ShowAction = true;
              break;
            case 577://AddressException
              _title = 'תקלה בשמירת כתובת,';
              ShowAction = true;
              break;
            case 578://LanguagesException
              _title = 'תקלה בשמירת שפות,';
              ShowAction = true;
              break;
            case 579://EducationException
              _title = 'תקלה בשמירת השכלה,';
              ShowAction = true;
              break;
            case 580://WorkExperienceException
              _title = 'תקלה בשמירת נסיון תעסוקתי,';
              ShowAction = true;
              break;
            case 581://NullBankException
              _title = 'תקלה בשמירת פרטי בנק,';
              ShowAction = true;
              break;
            case 582://NullBranchException
              _title = 'תקלה בשמירת פרטי סניף,';
              ShowAction = true;
              break;
            case 583://RelativesException
              _title = 'תקלה בשמירת פרטי קרובים,';
              ShowAction = true;
              break;
          }

          if (ShowAction) {
            this.barCaller.PopMessage(false, _text, _title);
            return throwError(`${error.error} (${error.status})`);
          }
          if (error.error instanceof ErrorEvent) {
            this.barCaller.PopMessage(false, 'network error.', String(error.status));
            // A client-side or network error occurred. Handle it accordingly.
            //console.error('An error occurred:', error.error.message);
          } else {
            this.barCaller.PopMessage(false, 'backend error.', String(error.statusText));
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            //console.error(`Backend returned code ${error.status}, ` + `status was: ${error.statusText}`);
          }
          // return an observable with a user-facing error message
          return throwError('Something bad happened; please try again later.');
        }),
        finalize(() => {
          this.postMassage.toggleBlocking(false);
        })
      );
  }
}
