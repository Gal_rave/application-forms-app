import { Pipe, PipeTransform } from '@angular/core';
import { EmployerModel } from '../models/employer-model';

@Pipe({
  name: 'filterEmployers'
})
export class FilterEmployersPipe implements PipeTransform {

  transform(args: EmployerModel[]): EmployerModel[] {
    return args.filter(e => e.name !== null && e.deductionFileID !== null);
  }

}
