import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'deliveryStatus'
})
export class DeliveryStatusPipe implements PipeTransform {

  transform(deliveryStatus: number): string {
    switch (deliveryStatus) {
      case 0:///עובד הגיש טופס ראשון
        return 'טופס הוגש';
      case 1:///טופס נדחה ע"י המשתמש
        return 'הוחזר לתיקון';
      case 2:///עובד הגיש טופס שנית
        return 'הוגש שנית';
      case 3:///משתמש שינה ידנית סטטוס לנדחה
        return 'טופס נדחה';
      case 4:///באת אישור טופס
        return 'הושלם';
      case 5:///כאשר הסטטוס הגיש ידנית
        return 'טופס סגור';
      default:
        return 'לא הגיש';
    }

  }

}
