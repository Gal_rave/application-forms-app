import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lengthFilter'
})
export class LengthFilterPipe implements PipeTransform {

  transform(list: any[], args: any[]): boolean {
    const il = list.filter(item=>{
      const ok = args.filter(arg=> {
        return item[arg] !== null;
      });
      return ok.length === args.length;
    });
    return il.length > 0;
  }

}
