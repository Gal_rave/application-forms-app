import { Pipe, PipeTransform } from '@angular/core';
import { EnumItem } from '../interfaces/enum-item';

@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
  transform(enumType: unknown): EnumItem[] {

    const underscoreReplace = /[_]/igm;
    const zReplace = /[Z]/igm;
    const wReplace = /[W]/igm;
    const qReplace = /[Q]/igm;
    const dolarReplace = /[$]/igm;

    return Object.keys(enumType).filter(k => isNaN(parseInt(k))).map(item => {
      return {
        index: enumType[item],
        name: item.replace(underscoreReplace, ' ').replace(wReplace, '-').replace(zReplace, '/').replace(qReplace, '"').replace(dolarReplace, '.').replace('""', '"')
      }
    });
  }
}
