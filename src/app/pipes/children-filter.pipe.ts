import { Pipe, PipeTransform } from '@angular/core';
import { ChildModel } from '../models/child-model';

@Pipe({
  name: 'childrenFilter'
})
export class ChildrenFilterPipe implements PipeTransform {

  transform(Children: Array<ChildModel>, filter: string, value: number): Array<ChildModel> {
    const date = new Date().getFullYear() - value;
    return Children.filter(c => c[filter].getFullYear() >= date);
  }

}
