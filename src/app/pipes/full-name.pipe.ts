import { Pipe, PipeTransform } from '@angular/core';
import { EmployeeModel } from '../models/employee-model';

@Pipe({
  name: 'fullName',pure: false
})
export class FullNamePipe implements PipeTransform {

  transform(usr: EmployeeModel): string {
    return `${usr.firstName} ${usr.lastName}`;
  }

}
