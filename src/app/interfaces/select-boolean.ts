export interface SelectBoolean {
    value?: boolean;
    name: string;
}
