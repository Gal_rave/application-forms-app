export interface SelectString {
    value: string;
    name: string;
}
