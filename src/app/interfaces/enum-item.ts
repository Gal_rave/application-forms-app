export interface EnumItem {
    index: number;
    name: string;
}
