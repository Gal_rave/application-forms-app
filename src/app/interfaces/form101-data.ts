export interface Form101Data {
    // ID: number;
    // actualcreationdate: Date;
    // createdDate: Date;
    // deliveryStatus: number;
    // formImageId: number;
    // jsonData: string;
    // municipalityId: number;
    // signatureImagId: number;
    // type: number;
    // userId: number;
    // version: string;

    ID: number;
    ACTUALCREATIONDATE: Date;
    CREATEDDATE: Date;
    DELIVERYSTATUS: number;
    FORMIMAGEID: number;
    JSONDATA: string;
    MUNICIPALITYID: number;
    SIGNATUREIMAGID: number;
    TYPE: number;
    USERID: number;
    VERSION: string;
}
