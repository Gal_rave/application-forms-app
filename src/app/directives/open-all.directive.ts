import { Directive, ElementRef, Renderer2, HostListener, AfterViewChecked } from '@angular/core';

@Directive({
  selector: '[OpenAll]'
})
export class OpenAllDirective implements AfterViewChecked {

  private Element: HTMLElement;
  private FaElement: HTMLElement;
  private childElements: NodeListOf<Element>;
  private StartOption: boolean = true;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    this.Element = el.nativeElement;
    this.renderer.addClass(this.Element, 'pointer');

    this.FaElement = renderer.createElement('i');
    this.renderer.addClass(this.FaElement, 'fa');
    this.renderer.addClass(this.FaElement, 'fa-plus-square');
    this.renderer.addClass(this.FaElement, 'fa-fw');
    this.renderer.setAttribute(this.FaElement, 'aria-hidden', 'true');
    this.renderer.setAttribute(this.FaElement, 'title', 'פתח הכל');
    this.renderer.insertBefore(this.Element, this.FaElement, this.Element.firstChild);
  }
  ngAfterViewChecked() {
    this.childElements = this.Element.parentElement.querySelectorAll('.col-xl-12 > .card-title > .fa-plus-square');
  }

  @HostListener('click') onClick() {
    this.StartOption = !this.StartOption;
    this.StartOption ? this.close() : this.open();
  }

  private open() {
    this.renderer.addClass(this.FaElement, 'active');

    this.childElements.forEach(el => {
      if (!el.classList.contains('active')) {
        (el as HTMLElement).click();
      }
    });
  }
  private close() {
    this.renderer.removeClass(this.FaElement, 'active');

    this.childElements.forEach(el => {
      if (el.classList.contains('active')) {
        (el as HTMLElement).click();
      }
    });
  }
}
