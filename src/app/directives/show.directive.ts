import { Directive, Input, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[shown]'
})
export class ShowDirective implements OnInit {
  @Input() public shown: boolean;
  constructor(private renderer: Renderer2, private elmRef: ElementRef) {
  }

  ngOnInit() {
    this.shown ? this.renderer.removeClass(this.elmRef.nativeElement, 'hide-element') :
      this.renderer.addClass(this.elmRef.nativeElement, 'hide-element');
  }

}
