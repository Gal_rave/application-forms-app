import { Directive, ElementRef, HostListener, Renderer2, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[FadeElement]'
})
export class FadeElementDirective implements OnInit {

  @Input() StartOption: boolean;
  @Input() fadeSelector: string;
  private Element: HTMLElement;
  private FadeElement: HTMLElement;
  private FaElement: HTMLElement;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    this.Element = el.nativeElement;
    this.renderer.addClass(this.Element, 'pointer');

    this.FaElement = renderer.createElement('i');
    this.renderer.addClass(this.FaElement, 'fa');
    this.renderer.addClass(this.FaElement, 'fa-plus-square');
    this.renderer.addClass(this.FaElement, 'fa-fw');
    this.renderer.setAttribute(this.FaElement, 'aria-hidden', 'true');
    this.renderer.insertBefore(this.Element, this.FaElement, this.Element.firstChild);
  }

  ngOnInit() {
    this.FadeElement = this.Element.parentElement.querySelector(this.fadeSelector);
    this.renderer.addClass(this.FadeElement, 'fadeout')
    if (!this.StartOption)
      this.IsFalse();
  }

  @HostListener('click') onClick() {
    this.StartOption = !this.StartOption;
    this.StartOption ? this.IsTrue() : this.IsFalse();
  }
  
  private IsTrue():void{
    this.renderer.removeClass(this.FadeElement, 'fadein');
    this.renderer.removeClass(this.FaElement, 'active');
  }
  private IsFalse():void{
    this.renderer.addClass(this.FadeElement, 'fadein');
    this.renderer.addClass(this.FaElement, 'active');
  }

}
