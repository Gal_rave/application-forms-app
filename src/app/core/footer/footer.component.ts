import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pager } from 'src/app/models/pager';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  @Input() pager: Pager;
  @Output("PageMove") PageMove = new EventEmitter<any>();

  public MovePage(move: any): void {
    this.PageMove.emit(move);
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
