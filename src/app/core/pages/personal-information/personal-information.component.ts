import { Component, Input, ViewChild, ViewChildren, QueryList, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

/*classes*/
import { ApplicantForm } from 'src/app/models/applicant-form';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { FamilyDetailsComponent } from '../LIstsComponents/family-details/family-details.component';
import { ChildrenComponent } from '../LIstsComponents/children/children.component';
import { LanguagesComponent } from '../LIstsComponents/languages/languages.component';
import { CoursesComponent } from '../LIstsComponents/courses/courses.component';
import { ExperienceComponent } from '../LIstsComponents/experience/experience.component';
import { GeneralInformationComponent } from '../LIstsComponents/general-information/general-information.component';
import { KaTavla } from 'src/app/models/ka-tavla';
import { ShowForms } from 'src/app/models/show-forms';
import { RelativesComponent } from '../ListsComponents/relatives/relatives.component';
import { ReferencesComponent } from '../ListsComponents/references/references.component';
import { EmergencyContactsComponent } from '../LIstsComponents/emergency-contacts/emergency-contacts.component';

@Component({
  selector: 'personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css']
})
export class PersonalInformationComponent implements OnInit {
  @ViewChild('PersonalInformationForm', { read: NgForm, static: false }) PersonalInformationForm: NgForm;

  @ViewChild(FamilyDetailsComponent) FamilyDetailsView: FamilyDetailsComponent;
  @ViewChild(GeneralInformationComponent) GIView: GeneralInformationComponent;

  @ViewChildren(ChildrenComponent) ChildrenViews: QueryList<ChildrenComponent>;
  @ViewChildren(LanguagesComponent) LanguagesViews: QueryList<LanguagesComponent>;
  @ViewChildren(CoursesComponent) CoursesViews: QueryList<CoursesComponent>;
  @ViewChildren(ExperienceComponent) ExperienceViews: QueryList<ExperienceComponent>;
  @ViewChildren(RelativesComponent) RelativesViews: QueryList<RelativesComponent>;
  @ViewChildren(ReferencesComponent) ReferencesViews: QueryList<ReferencesComponent>;
  @ViewChildren(EmergencyContactsComponent) EmergencyContactsViews: QueryList<EmergencyContactsComponent>;

  public Genders: SelectBoolean[] = [
    { value: null, name: 'לא רלוונטי' },
    { value: true, name: 'זכר' },
    { value: false, name: 'נקבה' }
  ];
  public Resident: SelectBoolean[] = [
    { value: true, name: 'כן' },
    { value: false, name: 'לא' }
  ];
  /*Degrees=> 1096, KinshipTypes=> 1039 */
  public Degrees: Array<KaTavla>;
  public KinshipTypes: Array<KaTavla>;

  @Input() personalInformation: ApplicantForm;
  @Input() DisableAll: boolean = false;
  @Input() ToggleAction: boolean;

  @Input() KaData: Array<KaTavla>;
  @Input() formOptions: ShowForms;

  ngOnInit() {
    this.Degrees = this.FilterKaTable(1096);
    this.KinshipTypes = this.FilterKaTable(1039);
  }
  CheckFormValidation(): boolean {
    if (this.PersonalInformationForm === undefined)
      return false;

    if (this.PersonalInformationForm.invalid)
      Object.keys(this.PersonalInformationForm.controls).forEach(key => {
        this.PersonalInformationForm.controls[key].markAsDirty();
      });
    /*validate all children*/
    if (this.ChildrenViews !== undefined && this.ChildrenViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
    /*validate all languages*/
    if (this.LanguagesViews !== undefined && this.LanguagesViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
    /*validate all courses*/
    if (this.CoursesViews !== undefined && this.CoursesViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
    /*validate all experiences (prev work experience)*/
    if (this.ExperienceViews !== undefined && this.ExperienceViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
    /*validate all relatives (relatives in municipality)*/
    if (this.RelativesViews !== undefined && this.RelativesViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
    /*validate all references (work references)*/
    if (this.ReferencesViews !== undefined && this.ReferencesViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
    /*validate all EmergencyContacts*/
    if (this.EmergencyContactsViews !== undefined && this.EmergencyContactsViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;

    return this.PersonalInformationForm.invalid
      || (this.FamilyDetailsView !== undefined && this.FamilyDetailsView.CheckFormValidation())
      || (this.GIView !== undefined && this.GIView.CheckFormValidation());
  }
  private FilterKaTable(tavle: number): Array<KaTavla> {
    return this.KaData.filter(ka => ka.PNH_MISP_TAVLA === tavle);
  }
}
