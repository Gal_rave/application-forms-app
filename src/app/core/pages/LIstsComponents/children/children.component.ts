import { Component, Input, ViewChild } from '@angular/core';

/*classes*/
import { ChildModel } from 'src/app/models/child-model';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'list-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.css']
})
export class ChildrenComponent {
  @ViewChild('ChildrenForm', { read: NgForm, static: false }) ChildrenForm: NgForm;
  
  public YesNo: SelectBoolean[] = [
    { value: true, name: 'כן' },
    { value: false, name: 'לא' }
  ];
  public Gender: SelectBoolean[] = [
    { value: true, name: 'זכר' },
    { value: false, name: 'נקבה' }
  ];

  @Input() child: ChildModel;
  @Input() DisableAll: boolean = false;

  CheckFormValidation(): boolean {
    if (this.ChildrenForm === undefined)
      return false;
      if (this.ChildrenForm.invalid)
      Object.keys(this.ChildrenForm.controls).forEach(key => {
        this.ChildrenForm.controls[key].markAsDirty();
      });
    return this.ChildrenForm.invalid;
  }
}
