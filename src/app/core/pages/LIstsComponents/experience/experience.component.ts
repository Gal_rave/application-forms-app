import { Component, Input, ViewChild } from '@angular/core';
/*classes*/
import { ApplicantEmployer } from 'src/app/models/applicant-employer';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'list-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent {
  @ViewChild('ExperienceForm', { read: NgForm, static: false }) ExperienceForm: NgForm;
  
  @Input() Employer: ApplicantEmployer;

  CheckFormValidation(): boolean {
    if (this.ExperienceForm === undefined)
      return false;
      if (this.ExperienceForm.invalid)
      Object.keys(this.ExperienceForm.controls).forEach(key => {
        this.ExperienceForm.controls[key].markAsDirty();
      });
    return this.ExperienceForm.invalid;
  }

  public get insertID(): string{
    return String(Math.abs(this.Employer.ID));
  }
}
