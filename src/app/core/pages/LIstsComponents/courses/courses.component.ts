import { Component, Input, ViewChild } from '@angular/core';
/*classes*/
import { Course } from 'src/app/models/course';
import { Education } from 'src/app/enums/education.enum';
import { NgForm } from '@angular/forms';
import { KaTavla } from 'src/app/models/ka-tavla';

@Component({
  selector: 'list-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent {
  @ViewChild('CoursesForm', { read: NgForm, static: false }) CoursesForm: NgForm;
  
  public educationEnum = Education;

  @Input() course: Course;
  @Input() Degrees: Array<KaTavla>;

  CheckFormValidation(): boolean {
    if (this.CoursesForm === undefined)
      return false;
      if (this.CoursesForm.invalid)
      Object.keys(this.CoursesForm.controls).forEach(key => {
        this.CoursesForm.controls[key].markAsDirty();
      });
    return this.CoursesForm.invalid;
  }

  public get insertID(): string{
    return String(Math.abs(this.course.ID));
  }
}
