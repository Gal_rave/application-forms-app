import { Component, Input, ViewChild } from '@angular/core';
/*classes*/
import { ApplicantForm } from 'src/app/models/applicant-form';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { IdfRanks } from 'src/app/enums/idf-ranks.enum';
import { Hmos } from 'src/app/enums/hmos.enum';
import { OrganizationalAffiliation } from 'src/app/enums/organizational-affiliation.enum';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'general-information',
  templateUrl: './general-information.component.html',
  styleUrls: ['./general-information.component.css']
})
export class GeneralInformationComponent {
  @ViewChild('GIForm', { read: NgForm, static: false }) GIForm: NgForm;

  public Ranks = IdfRanks;
  public hmos = Hmos;
  public Organizations = OrganizationalAffiliation;

  public Resident: SelectBoolean[] = [
    { value: true, name: 'כן' },
    { value: false, name: 'לא' }
  ];
  public ServiceType: SelectBoolean[] = [
    { value: true, name: 'צבאי' },
    { value: false, name: 'לאומי' }
  ];

  @Input() applicantForm: ApplicantForm;

  CheckFormValidation(): boolean {
    if (this.GIForm === undefined)
      return false;
      if (this.GIForm.invalid)
      Object.keys(this.GIForm.controls).forEach(key => {
        this.GIForm.controls[key].markAsDirty();
      });
    return this.GIForm.invalid;
  }
}
