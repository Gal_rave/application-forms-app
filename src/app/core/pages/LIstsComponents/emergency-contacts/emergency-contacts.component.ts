import { Component, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EmergencyContactModel } from 'src/app/models/emergency-contact-model';

@Component({
  selector: 'list-emergency-contacts',
  templateUrl: './emergency-contacts.component.html',
  styleUrls: ['./emergency-contacts.component.css']
})
export class EmergencyContactsComponent {
  @ViewChild('EmergencyContactForm', { read: NgForm, static: false }) EmergencyContactForm: NgForm;

  @Input() EmergencyContact: EmergencyContactModel;

  cityChanged(): void {
    this.EmergencyContact.cityID = -1;
  }

  CheckFormValidation(): boolean {
    if (this.EmergencyContactForm === undefined)
      return false;
    if (this.EmergencyContactForm.invalid)
      Object.keys(this.EmergencyContactForm.controls).forEach(key => {
        this.EmergencyContactForm.controls[key].markAsDirty();
      });
    return this.EmergencyContactForm.invalid;
  }

  public get insertID(): string {
    return String(Math.abs(this.EmergencyContact.ID));
  }
}
