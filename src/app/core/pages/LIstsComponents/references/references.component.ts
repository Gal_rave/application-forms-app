import { Component, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ReferenceModel } from 'src/app/models/reference-model';

@Component({
  selector: 'list-references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.css']
})
export class ReferencesComponent {
  @ViewChild('ReferenceForm', { read: NgForm, static: false }) ReferenceForm: NgForm;

  @Input() Reference: ReferenceModel;

  CheckFormValidation(): boolean {
    if (this.ReferenceForm === undefined)
      return false;
      if (this.ReferenceForm.invalid)
      Object.keys(this.ReferenceForm.controls).forEach(key => {
        this.ReferenceForm.controls[key].markAsDirty();
      });
    return this.ReferenceForm.invalid;
  }

  public get insertID(): string{
    return String(Math.abs(this.Reference.ID));
  }
}
