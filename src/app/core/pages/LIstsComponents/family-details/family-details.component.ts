import { Component, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
/*classes*/
import { ApplicantForm } from 'src/app/models/applicant-form';
import { MaritalStates } from 'src/app/enums/marital-states.enum';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';

@Component({
  selector: 'family-details',
  templateUrl: './family-details.component.html',
  styleUrls: ['./family-details.component.css']
})
export class FamilyDetailsComponent {
  @ViewChild('FamilyDetailsForm', { read: NgForm, static: false }) FamilyDetailsForm: NgForm;
  
  public maritalStatesEnum = MaritalStates;
  public Resident: SelectBoolean[] = [
    { value: true, name: 'כן' },
    { value: false, name: 'לא' }
  ];

  @Input() applicantForm: ApplicantForm;

  CheckFormValidation(): boolean {
    if (this.FamilyDetailsForm === undefined)
      return false;
      if (this.FamilyDetailsForm.invalid)
      Object.keys(this.FamilyDetailsForm.controls).forEach(key => {
        this.FamilyDetailsForm.controls[key].markAsDirty();
      });
    return this.FamilyDetailsForm.invalid;
  }
}
