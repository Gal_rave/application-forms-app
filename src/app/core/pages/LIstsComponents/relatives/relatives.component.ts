import { Component, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatOptionSelectionChange } from '@angular/material/core';
import { KaTavla } from 'src/app/models/ka-tavla';
import { RelativeModel } from 'src/app/models/relative-model';

@Component({
  selector: 'list-relatives',
  templateUrl: './relatives.component.html',
  styleUrls: ['./relatives.component.css']
})
export class RelativesComponent {
  @ViewChild('RelativeForm', { read: NgForm, static: false }) RelativeForm: NgForm;

  @Input() Relative: RelativeModel;
  @Input() KinshipTypes: Array<KaTavla>;

  CheckFormValidation(): boolean {
    if (this.RelativeForm === undefined)
      return false;
    if (this.RelativeForm.invalid)
      Object.keys(this.RelativeForm.controls).forEach(key => {
        this.RelativeForm.controls[key].markAsDirty();
      });
    return this.RelativeForm.invalid;
  }

  SelectionChange(event: MatOptionSelectionChange, KinshipCode: KaTavla): void {
    if (!event.isUserInput)
      return;
    this.Relative.KinshipText = KinshipCode.PNH_TEUR_K;
  }

  public get insertID(): string {
    return String(Math.abs(this.Relative.ID));
  }
}
