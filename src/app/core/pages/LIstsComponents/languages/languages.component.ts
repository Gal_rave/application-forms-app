import { Component, Input, ViewChild, OnInit } from '@angular/core';
/*classes*/
import { Language } from 'src/app/models/language';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { LanguageLevels } from 'src/app/enums/language-levels.enum';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'list-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.css']
})
export class LanguagesComponent {
  @ViewChild('LanguagesForm', { read: NgForm, static: false }) LanguagesForm: NgForm;
  
  public YesNo: SelectBoolean[] = [
    {value: true, name: 'כן'},
    {value: false, name: 'לא'}
  ];
  public Levels = LanguageLevels;
  
  @Input() language: Language;
  
  CheckFormValidation(): boolean {
    if (this.LanguagesForm === undefined)
      return false;
      if (this.LanguagesForm.invalid)
      Object.keys(this.LanguagesForm.controls).forEach(key => {
        this.LanguagesForm.controls[key].markAsDirty();
      });
    return this.LanguagesForm.invalid;
  }

  public get insertID(): string{
    return String(Math.abs(this.language.ID));
  }
}
