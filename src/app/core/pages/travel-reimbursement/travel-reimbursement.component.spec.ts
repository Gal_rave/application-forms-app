import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelReimbursementComponent } from './travel-reimbursement.component';

describe('TravelReimbursementComponent', () => {
  let component: TravelReimbursementComponent;
  let fixture: ComponentFixture<TravelReimbursementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelReimbursementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelReimbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
