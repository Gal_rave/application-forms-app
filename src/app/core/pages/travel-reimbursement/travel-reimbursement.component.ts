import { Component, Input, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';

/*classes*/
import { Travel } from 'src/app/models/travel';
import { TransportModeEnum } from 'src/app/enums/transport-modes.enum';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'travel-reimbursement',
  templateUrl: './travel-reimbursement.component.html',
  styleUrls: ['./travel-reimbursement.component.css']
})
export class TravelReimbursementComponent {
  @ViewChild('TravelReimbursementForm', { read: NgForm, static: false }) TravelReimbursementForm: NgForm;

  public transportModes = TransportModeEnum;
  @Input() travelReimbursement: Travel;
  @Input() ToggleAction: boolean;

  CheckFormValidation(): boolean {
    if (this.TravelReimbursementForm === undefined)
      return false;
    if (this.TravelReimbursementForm.invalid)
      Object.keys(this.TravelReimbursementForm.controls).forEach(key => {
        this.TravelReimbursementForm.controls[key].markAsDirty();
      });
    return this.TravelReimbursementForm.invalid;
  }
}
