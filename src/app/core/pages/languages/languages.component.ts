import { Component, OnInit, Input } from '@angular/core';
/*classes*/
import { Language } from 'src/app/models/language';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { LanguageLevels } from 'src/app/enums/language-levels.enum';

@Component({
  selector: 'list-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.css']
})
export class LanguagesComponent {
  public YesNo: SelectBoolean[] = [
    {value: true, name: 'כן'},
    {value: false, name: 'לא'}
  ];
  public Levels = LanguageLevels;
  
  @Input() language: Language;
  
}
