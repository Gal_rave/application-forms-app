import { Component, AfterViewChecked, ViewChildren, QueryList } from '@angular/core';

/*services*/
import { FormsService } from 'src/app/services/forms-service';
import { DataService } from 'src/app/services/data.service';
import { ParentMassageService } from 'src/app/services/parent-massage.service';
import { SnackBarCaller } from 'src/app/custom-material/snack-bar-caller';
import { ExcelService } from 'src/app/services/excel.service';

/*classes*/
import { FormData } from 'src/app/models/form-data';
import { Filter } from 'src/app/models/filter';
import { SortModel } from 'src/app/models/sort-model';
import { ShowFormComponent } from '../show-form/show-form.component';
import { KaTavla } from 'src/app/models/ka-tavla';
import { Pager } from 'src/app/models/pager';
import { FactoryModel } from 'src/app/models/factory-model';
import { BudgetItem } from 'src/app/models/budget-item';
import { DeliveryStatusEnum } from 'src/app/enums/delivery-status.enum';

@Component({
  selector: 'app-main-index',
  templateUrl: './main-index.component.html',
  styleUrls: ['./main-index.component.css']
})
export class MainIndexComponent implements AfterViewChecked {
  private pikerArray: Array<HTMLElement> = new Array<HTMLElement>();
  private deliveryStatusEnum = DeliveryStatusEnum;
  private rashutFactorys: Array<FactoryModel>;
  private budgetItems: Array<BudgetItem>;
  private formName = {
    669: 'טופס 101',
    1: 'פרטים אישיים',
    2: 'החזר נסיעות',
    3: 'הצטרפות להסתדרות',
    4: 'קופת גמל',
    5: 'טופסי אישור'
  };

  public forms: Array<FormData>;
  public Showforms: Array<FormData>;

  public KaTavlaData: Array<KaTavla> = new Array<KaTavla>();
  public pager: Pager;
  public pagedForms: Array<Array<FormData>>;
  public ShowFactorys: boolean = false;
  public ShowBudgetItems: boolean = false;

  @ViewChildren(ShowFormComponent) ChildrenViews: QueryList<ShowFormComponent>;
  private formsToUpdate: Array<ShowFormComponent> = new Array<ShowFormComponent>();

  constructor(private formsService: FormsService, private dataService: DataService, private parentMassage: ParentMassageService, private barCaller: SnackBarCaller, private excelService: ExcelService) {
    formsService.ClearFormsValue();
    /*set up component base*/
    this.pager = new Pager([]);
    this.pagedForms = new Array<Array<FormData>>();
    this.Showforms = new Array<FormData>();

    dataService.GetRashutFactorys().subscribe((data: Array<FactoryModel>) => {
      this.ShowFactorys = data.length > 1;
      this.rashutFactorys = data;
      setTimeout(() => {
        parentMassage.AddChatMessage('GetRashutFactorys');
      }, 350);
    });
    dataService.GetBudgetItems().subscribe((data: Array<BudgetItem>) => {
      this.ShowBudgetItems = data.length > 1;
      this.budgetItems = data;
      setTimeout(() => {
        parentMassage.AddChatMessage('GetBudgetItems');
      }, 350);
    });
    formsService.GetBase(1096).subscribe((data: Array<KaTavla>) => {
      this.KaTavlaData = this.KaTavlaData.concat(data);
    });
    formsService.GetBase(1039).subscribe((data: Array<KaTavla>) => {
      this.KaTavlaData = this.KaTavlaData.concat(data);
    });

    formsService.GetApplicants().subscribe((rfms: Array<FormData>) => {
      this.forms = rfms;
      this.Showforms = rfms;
      this.pager = new Pager(rfms);
      this.pagedForms = this.chunkArray(rfms, this.pager.PageSize);
    });
  }

  ngAfterViewChecked() {
    const datePickers = $('input[bsdatepicker]');
    if (datePickers.length > 0) {
      for (const [key, elem] of Object.entries(datePickers)) {
        if (elem instanceof HTMLElement && this.pikerArray.indexOf(elem) < 0) {
          elem.removeEventListener('click', this.HandleDatepickerClick, true);
          elem.addEventListener('click', this.HandleDatepickerClick.bind(this), true);
          this.pikerArray.push(elem);
        }
      }
    }
  }

  public MovePage(move: any): void {
    if (isNaN(move)) {
      switch (move) {
        case '+':
          this.pager.PageNumber += 1;
          break;
        case '++':
          this.pager.PageNumber = (this.pager.MaxPages - 1);
          break;
        case '-':
          this.pager.PageNumber -= 1;
          break;
        case '--':
          this.pager.PageNumber = 0;
          break;
      }
    } else {
      this.pager.PageNumber = move;
    }
  }

  public HandleDatepickerClick(event: any): void {
    this.parentMassage.CallReLoadIframe(0);
  }

  public OnFormOpenAction(action: boolean) {
    this.parentMassage.CallReLoadIframe(0);
    if (!action) {
      this.pikerArray = new Array<HTMLElement>();
    }
  }

  ExportExcelFile(event: boolean): void {
    const dataArray: Array<any> = new Array<any>();
    const dataObject: object = {
      COMMENTTEXT: 'הערה',
      CONFIRMATION: 'אישור כ"א',
      CONFIRMATIONDATE: 'ת. אישור כ"א',
      PERSONNELAPPROVAL_USERNAME: 'מעדכן כ"א',
      CREATEDDATE: 'ת. יצירה',
      DELIVERYSTATUS: 'סטטוס טופס',
      ERU_KAV_ASMACHTA: 'מס" אסמכתא',
      ERU_OVD_KTV_EMAIL: 'דוא"ל',
      SEIF_TAKZIVI_RASHI: 'סעיף תקציבי',
      UPDATEDATE: 'ת. עדכון אחרון',
      UPDATINGUSER: 'מעדכן',
      UPDATINGUSERDATE: 'ת.עדכון',
      UPDATINGUSER_USERNAME: 'שם מעדכן',
      USERID: 'מס" עובד',
      USERNAME: 'שם עובד',
      PERSONNELAPPROVAL: 'אישור קליטה',
      PERSONNELAPPROVALDATE: 'ת. אישור קליטה',
      MIFAL: 'מפעל',
      formsCounter: 'מס" טפסים',
      MunicipalityForms: 'טפסים'
    };
    let obj;
    const ObjectKeys = Object.keys(dataObject);

    this.Showforms.forEach(f => {
      obj = {};
      ObjectKeys.forEach(key => {
        obj[dataObject[key]] = this.translateData(key, f);
      });
      dataArray.push(Object.assign({}, obj));
    });
    this.excelService.ExportAsExcelFile(dataArray, 'Showforms');
  }
  private translateData(key: string, data: FormData): any {
    const Val = data[key];
    switch (key) {
      case 'CONFIRMATION':
        return Val === 1 ? 'כן' : 'לא';

      case 'DELIVERYSTATUS':
        return this.deliveryStatusEnum[Val];

      case 'SEIF_TAKZIVI_RASHI':
        if (Val !== null && Val > 0) {
          const retVal = this.budgetItems.find(r => r.code === Val);
          return retVal && retVal.description || Val;
        }
        return Val;

      case 'PERSONNELAPPROVAL':
        if (Val === 2 && data.IFN_TRANSACTIONID !== null) {
          return 'קליטת נתונים הסתיימה';
        }
        if (Val === 0 && data.DELIVERYSTATUS === 4) {
          return 'ניתן לבצע קליטת נתונים';
        }
        return '';

      case 'PERSONNELAPPROVALDATE':
        if (data.PERSONNELAPPROVAL === 2 && data.IFN_TRANSACTIONID !== null) {
          return Val;
        }
        return null;

      case 'MIFAL':
        if (Val !== null && Val > 0) {
          const retVal = this.rashutFactorys.find(r => r.code === Val);
          return retVal && retVal.name || Val;
        }
        return Val;

      case 'MunicipalityForms':
        if (data.formsCounter > 0) {
          return data.AppData.MunicipalityForms.map((f: number) => this.formName[f]).join(',');
        }
        return '';

      case 'CONFIRMATIONDATE':
        if (data.CONFIRMATION === 1) {
          return Val;
        }
        return null;

      default:
        return Val;
    }
  }

  UpdateStutas(event: boolean): void {
    this.formsToUpdate = new Array<ShowFormComponent>();
    if (this.ChildrenViews === undefined) {
      this.barCaller.PopMessage(false, 'התרחשה תקלה!');
      return;
    }

    let Valid = false;
    this.ChildrenViews.forEach(ic => {
      if (ic.CheckFormValidation()) {
        Valid = true;
      }
    });
    if (Valid) {
      this.barCaller.PopMessage(false, 'שגיאת נתונים!', 'שדות חובה אינם תקינים.');
      return;
    }

    this.formsToUpdate = this.ChildrenViews.filter(frm => !frm.ShowFormComponentForm.pristine && frm.ShowFormComponentForm.touched);
    if (this.formsToUpdate.length === 0) {
      this.barCaller.PopMessage(false, 'אין נתונים לעדכון!', 'בצע עדכון לטפסים.');
      return;
    }

    this.formsService.UpDateForms(this.formsToUpdate.map(frm => frm.form)).subscribe(frms => {
      if (typeof frms === 'object' && frms instanceof Array) {
        this.formsToUpdate.map(frm => {
          frm.form = frms.find(f => f.ID === frm.form.ID);
        });
        this.barCaller.PopMessage(true, 'נתונים נשמרו בהצלחה!');
        this.ChildrenViews.filter(frm => !frm.ShowFormComponentForm.pristine && frm.ShowFormComponentForm.touched).forEach(frm => {
          frm.ShowFormComponentForm.form.markAsPristine();
          frm.ShowFormComponentForm.form.markAsUntouched();
        });
      } else {
        this.barCaller.PopMessage(false, 'התרחשה תקלה בשמירת הנתונים!');
      }
    });
  }

  FilterFroms(event: Filter) {
    this.Showforms = this.forms;
    // tslint:disable-next-line: forin
    for (let i in event) {
      switch (this.getTypeOfItem(event[i])) {
        case 0:
          this.filterString(i, event[i]);
          break;
        case 3:
          if (event[i] instanceof Array && event[i].length > 0) {
            let listitems: Array<FormData> = new Array<FormData>();
            event[i].forEach(element => {
              switch (this.getTypeOfItem(element)) {
                case 0:
                  listitems = [...listitems, ...this.filterMultipleString(this.Showforms, i, element)];
                  break;
                case 1:
                  listitems = [...listitems, ...this.filterMultipleNumber(this.Showforms, i, element)];
                  break;
              }
            });
            this.Showforms = listitems;
          }
          break;
      }
    }
    this.pager = new Pager(this.Showforms);
    this.pagedForms = this.chunkArray(this.Showforms, this.pager.PageSize);
  }

  SortForms(sortItem: SortModel) {
    this.Showforms = this.Showforms.sort((a, b) => {
      if (this.getTypeOfItem(a[sortItem.SortItem]) === 0) {
        return sortItem.SortDirection ? ('' + a[sortItem.SortItem]).localeCompare(b[sortItem.SortItem]) : ('' + b[sortItem.SortItem]).localeCompare(a[sortItem.SortItem]);
      }
      return sortItem.SortDirection ? a[sortItem.SortItem] - b[sortItem.SortItem] : b[sortItem.SortItem] - a[sortItem.SortItem];
    });
    this.pagedForms = this.chunkArray(this.Showforms, this.pager.PageSize);
  }

  private chunkArray(arr: Array<any>, n: number): Array<any> {
    const chunks = new Array<Array<any>>();
    let i = 0;
    while (arr.length > (n * i)) {
      chunks.push(arr.slice(n * i, n * (++i)));
    }
    return chunks;
  }
  private getTypeOfItem(item: string): number {
    switch (typeof item) {
      case 'symbol':
      case 'string':
        return 0;
      case 'number':
      case 'bigint':
        return 1;
      case 'boolean':
        return 2;
      case 'object':
        return 3;
      case 'undefined':
        return 4;
    }
    return -1;
  }
  private filterString(item: string, value: string): void {
    this.Showforms = this.Showforms.filter(f => String(f[item]).includes(value));
  }
  private filterNumber(item: string, value: number): void {
    this.Showforms = this.Showforms.filter(f => f[item] === value);
  }
  private filterMultipleString(filteritems: Array<FormData>, item: string, value: string): Array<FormData> {
    return filteritems.filter(f => String(f[item]).includes(value));
  }
  private filterMultipleNumber(filteritems: Array<FormData>, item: string, value: number): Array<FormData> {
    return filteritems.filter(f => f[item] === value);
  }

}
