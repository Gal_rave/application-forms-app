import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ProvidentFund } from 'src/app/models/provident-fund';
import { PosTypes } from 'src/app/enums/pos-types.enum';
import { DefaultPosTypes } from 'src/app/enums/default-pos-types.enum';

@Component({
  selector: 'provident-fund',
  templateUrl: './provident-fund.component.html',
  styleUrls: ['./provident-fund.component.css']
})
export class ProvidentFundComponent implements OnInit {
  @ViewChild('ProvidentFundForm', { read: NgForm, static: false }) ProvidentFundForm: NgForm;

  public posTypes = PosTypes;
  public defaultPosTypes = DefaultPosTypes;

  @Input() DisableAll: boolean = false;
  @Input() ToggleAction: boolean;
  @Input() Funds: Array<ProvidentFund>;

  ngOnInit(): void {
    this.Funds.forEach(f => {
      f.ID = Math.abs(f.ID);
      f.TreasuryNumber = f.TreasuryNumber !== undefined && f.TreasuryNumber !== null && f.TreasuryNumber.length > 1? f.TreasuryNumber : null;
      f.TreasuryCode = f.TreasuryCode > 0? f.TreasuryCode : null;
    });
  }

  CheckFormValidation(): boolean {
    if (this.ProvidentFundForm === undefined)
      return false;
    if (this.ProvidentFundForm.invalid)
      Object.keys(this.ProvidentFundForm.controls).forEach(key => {
        this.ProvidentFundForm.controls[key].markAsDirty();
      });
    return this.ProvidentFundForm.invalid;
  }

}
