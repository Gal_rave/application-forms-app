import { Component, Input, OnInit, Output, EventEmitter, SimpleChange, ViewChild } from '@angular/core';

/*classes*/
import { ApplicantForm } from 'src/app/models/applicant-form';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { SupplicateOptions } from 'src/app/enums/supplicate-options.enum';
import { PaymentOptions } from 'src/app/enums/payment-options.enum';
import { OrganizationalAffiliation } from 'src/app/enums/organizational-affiliation.enum';
import { NgForm } from '@angular/forms';
import { strict } from 'assert';

@Component({
  selector: 'workers-union',
  templateUrl: './workers-union.component.html',
  styleUrls: ['./workers-union.component.css']
})
export class WorkersUnionComponent implements OnInit {
  @ViewChild('WorkersUnionForm', { read: NgForm, static: false }) WorkersUnionForm: NgForm;

  public FormType: boolean;
  public supplicateOptions = SupplicateOptions;
  public paymentOptions = PaymentOptions;
  public Organizations = OrganizationalAffiliation;

  public Genders: SelectBoolean[] = [
    { value: null, name: 'לא רלוונטי' },
    { value: true, name: 'זכר' },
    { value: false, name: 'נקבה' }
  ];
  public Resident: SelectBoolean[] = [
    { value: true, name: 'כן' },
    { value: false, name: 'לא' }
  ];

  @Input() workersUnion: ApplicantForm;
  @Input() ToggleAction: boolean;

  ngOnInit(): void {
    this.FormType = this.workersUnion.employee.organizationalAffiliation === 2;
  }

  CheckFormValidation(): boolean {
    if (this.WorkersUnionForm === undefined)
      return false;
    if (this.WorkersUnionForm.invalid)
      Object.keys(this.WorkersUnionForm.controls).forEach(key => {
        this.WorkersUnionForm.controls[key].markAsDirty();
      });
    return this.WorkersUnionForm.invalid;
  }
}
