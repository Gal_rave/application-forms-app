import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkersUnionComponent } from './workers-union.component';

describe('WorkersUnionComponent', () => {
  let component: WorkersUnionComponent;
  let fixture: ComponentFixture<WorkersUnionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkersUnionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkersUnionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
