import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ApplicantForm } from 'src/app/models/applicant-form';
import { KaTavla } from 'src/app/models/ka-tavla';
import { EmergencyContactsComponent } from '../LIstsComponents/emergency-contacts/emergency-contacts.component';
import { ReferencesComponent } from '../ListsComponents/references/references.component';
import { RelativesComponent } from '../ListsComponents/relatives/relatives.component';

@Component({
  selector: 'references-and-relatives',
  templateUrl: './references-and-relatives.component.html',
  styleUrls: ['./references-and-relatives.component.css']
})
export class ReferencesAndRelativesComponent implements OnInit {
  @Input() applicantForm: ApplicantForm;
  /*Degrees=> 1096, KinshipTypes=> 1039 */
  @Input() KinshipTypes: Array<KaTavla>;
  @Input() ToggleAction: boolean;

  @ViewChildren(RelativesComponent) RelativesViews: QueryList<RelativesComponent>;
  @ViewChildren(ReferencesComponent) ReferencesViews: QueryList<ReferencesComponent>;
  @ViewChildren(EmergencyContactsComponent) EmergencyContactsViews: QueryList<EmergencyContactsComponent>;

  ngOnInit(): void {
    this.KinshipTypes = this.KinshipTypes.filter(ka => ka.PNH_MISP_TAVLA === 1039);
  }
  CheckFormValidation(): boolean {
    /*validate all relatives (relatives in municipality)*/
    if (this.RelativesViews !== undefined && this.RelativesViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
    /*validate all references (work references)*/
    if (this.ReferencesViews !== undefined && this.ReferencesViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
    /*validate all EmergencyContacts*/
    if (this.EmergencyContactsViews !== undefined && this.EmergencyContactsViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;
  }
}
