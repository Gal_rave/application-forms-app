import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferencesAndRelativesComponent } from './references-and-relatives.component';

describe('ReferencesAndRelativesComponent', () => {
  let component: ReferencesAndRelativesComponent;
  let fixture: ComponentFixture<ReferencesAndRelativesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferencesAndRelativesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferencesAndRelativesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
