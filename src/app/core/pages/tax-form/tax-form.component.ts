import { Component, Input, ViewChild, ViewChildren, QueryList } from '@angular/core';
/*classes*/
import { ApplicantForm } from 'src/app/models/applicant-form';
import { EmployeeDetailsComponent } from '../InnerTaxForm/employee-details/employee-details.component';
import { ChildrenComponent } from '../LIstsComponents/children/children.component';
import { TaxFamilyDetailsComponent } from '../InnerTaxForm/tax-family-details/tax-family-details.component';
import { IncomeComponent } from '../InnerTaxForm/income/income.component';

@Component({
  selector: 'tax-form',
  templateUrl: './tax-form.component.html',
  styleUrls: ['./tax-form.component.css']
})
export class TaxFormComponent {
  @ViewChild(EmployeeDetailsComponent) WorkersUnionView: EmployeeDetailsComponent;
  @ViewChild(TaxFamilyDetailsComponent) TFD_View: TaxFamilyDetailsComponent;
  @ViewChild(IncomeComponent) IncomeView: IncomeComponent;

  @Input() taxForm: ApplicantForm;
  @Input() DisableAll: boolean = false;
  @Input() ToggleAction: boolean;

  CheckFormValidation(): boolean {    
    return (this.WorkersUnionView !== undefined && this.WorkersUnionView.CheckFormValidation()) ||
          (this.TFD_View !== undefined && this.TFD_View.CheckFormValidation()) ||
          (this.IncomeView !== undefined && this.IncomeView.CheckFormValidation()) ;
  }
}
