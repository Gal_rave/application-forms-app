import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
/*classes*/
import { EmployerModel } from 'src/app/models/employer-model';
import { EmployerIncomeTypes } from 'src/app/enums/employer-income-types.enum';
import { SelectItem } from 'src/app/models/select-item';

@Component({
  selector: 'tax-coordination-employer',
  templateUrl: './tax-coordination-employer.component.html',
  styleUrls: ['./tax-coordination-employer.component.css']
})
export class TaxCoordinationEmployerComponent implements OnInit {
  @ViewChild('TCE_Form', { read: NgForm, static: false }) TCE_Form: NgForm;
  
  private IncomeTypeIds: SelectItem[] = [
    { Value: 'monthSalary', Key: 1 }, { Value: 'allowance', Key: 2 }, { Value: 'stipend', Key: 3 }, { Value: 'anotherSource', Key: 4 }
  ];

  public incomeTypes = EmployerIncomeTypes;

  @Input() Employer: EmployerModel;
  @Input() DisableAll: boolean = false;

  public incomeTypeID: number = 0;

  ngOnInit(): void {
    this.setIncomeTypeID();
  }

  CheckFormValidation(): boolean {
    if (this.TCE_Form === undefined)
      return false;
      if (this.TCE_Form.invalid)
      Object.keys(this.TCE_Form.controls).forEach(key => {
        this.TCE_Form.controls[key].markAsDirty();
      });
    return this.TCE_Form.invalid;
  }

  incomeTypeSelectionChange(val: number) {
    this.getIncomeTypeID(val);
  }

  private setIncomeTypeID(): void {
    const obj = Object.keys(this.Employer.incomeType).filter(k => this.IncomeTypeIds.find(e => e.Value === k) !== undefined && this.Employer.incomeType[k])
      .map(k => {
        return this.IncomeTypeIds.find(e => e.Value === k).Key;
      });

    this.incomeTypeID = (obj === undefined || obj.length < 1) ? 0 : obj[0];
  }
  private getIncomeTypeID(selected: number): void {
    const obj = this.IncomeTypeIds.find(e => e.Key === selected);
    if (obj !== undefined) {
      Object.keys(this.Employer.incomeType).filter(k => this.IncomeTypeIds.find(e => e.Value === k) !== undefined).map(k => this.Employer.incomeType[k] = false);
      this.Employer.incomeType[obj.Value] = true;
    }
  }

}
