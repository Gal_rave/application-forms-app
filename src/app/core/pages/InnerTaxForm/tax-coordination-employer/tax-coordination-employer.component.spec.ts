import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxCoordinationEmployerComponent } from './tax-coordination-employer.component';

describe('TaxCoordinationEmployerComponent', () => {
  let component: TaxCoordinationEmployerComponent;
  let fixture: ComponentFixture<TaxCoordinationEmployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxCoordinationEmployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxCoordinationEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
