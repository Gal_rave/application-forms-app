import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { NgForm } from '@angular/forms';
/*classes*/
import { ApplicantForm } from 'src/app/models/applicant-form';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { SelectItem } from 'src/app/models/select-item';

@Component({
  selector: 'app-income',
  templateUrl: './income.component.html',
  styleUrls: ['./income.component.css']
})
export class IncomeComponent {
  @ViewChild('IncomeForm', { read: NgForm, static: false }) IncomeForm: NgForm;

  public HasIncome: SelectBoolean[] = [
    { value: false, name: 'אין לי הכנסות אחרות לרבות מלגות' },
    { value: true, name: 'יש לי הכנסות נוספות כמפורט להלן' }
  ];
  public IncomeTaxCredit: SelectItem[] = [
    { Value: 'אבקש לקבל נקודות זיכוי ומדרגות מס כנגד הכנסתי זו (סעיף ד). איני מקבל/ת אותם בהכנסה אחרת.', Key: 1 },
    { Value: 'אני מקבל/ת נקודות זיכוי ומדרגות מס בהכנסה אחרת ועל כן איני זכאי/ת להם כנגד הכנסה זו.', Key: 2 }
  ];
  @Input() form: ApplicantForm;
  @Input() DisableAll: boolean = false;

  CheckFormValidation(): boolean {
    if (this.IncomeForm === undefined)
      return false;
    if (this.IncomeForm.invalid)
      Object.keys(this.IncomeForm.controls).forEach(key => {
        this.IncomeForm.controls[key].markAsDirty();
      });
    return this.IncomeForm.invalid;
  }

  incomeOptions(event: MatCheckboxChange, name: string): void {
    if (event.checked) {
      switch (name) {
        case 'monthSalary':
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          break;
        case 'anotherSalary':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          break;
        case 'partialSalary':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          break;
        case 'daySalary':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          break;
        case 'allowance':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.stipend = false;
          break;
        case 'stipend':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          break;
        default:
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          break;
      }
    }
  }

  otherIncomesOptions(event: MatCheckboxChange, name: string): void {
    if (event.checked) {
      switch (name) {
        case 'monthSalary':
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          this.form.otherIncomes.incomeType.anotherSource = false;
          break;
        case 'anotherSalary':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          this.form.otherIncomes.incomeType.anotherSource = false;
          break;
        case 'partialSalary':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          this.form.otherIncomes.incomeType.anotherSource = false;
          break;
        case 'daySalary':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          this.form.otherIncomes.incomeType.anotherSource = false;
          break;
        case 'allowance':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.stipend = false;
          this.form.otherIncomes.incomeType.anotherSource = false;
          break;
        case 'stipend':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.otherIncomes.incomeType.anotherSource = false;
          break;
        case 'anotherSource':
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          break;
        default:
          this.form.income.incomeType.monthSalary = false;
          this.form.income.incomeType.anotherSalary = false;
          this.form.income.incomeType.partialSalary = false;
          this.form.income.incomeType.daySalary = false;
          this.form.income.incomeType.allowance = false;
          this.form.income.incomeType.stipend = false;
          this.form.otherIncomes.incomeType.anotherSource = false;
          break;
      }
    }
  }

}
