import { Component, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
/*classes*/
import { ApplicantForm } from 'src/app/models/applicant-form';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { SelectItem } from 'src/app/models/select-item';

@Component({
  selector: 'income-tax-rebate',
  templateUrl: './income-tax-rebate.component.html',
  styleUrls: ['./income-tax-rebate.component.css']
})
export class IncomeTaxRebateComponent {
  @ViewChild('ITR_Form', { read: NgForm, static: false }) ITR_Form: NgForm;

  public Resident: SelectBoolean[] = [
    { value: true, name: 'כן' },
    { value: false, name: 'לא' }
  ];
  public immigrationStatus: SelectItem[] = [
    { Value: 'עולה חדש/ה', Key: 1 },
    { Value: 'תושב חוזר', Key: 2 }
  ];

  @Input() form: ApplicantForm;
  @Input() DisableAll: boolean = false;

  CheckFormValidation(): boolean {
    if (this.ITR_Form === undefined)
      return false;
    if (this.ITR_Form.invalid)
      Object.keys(this.ITR_Form.controls).forEach(key => {
        this.ITR_Form.controls[key].markAsDirty();
      });
    return this.ITR_Form.invalid;
  }
}
