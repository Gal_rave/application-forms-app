import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeTaxRebateComponent } from './income-tax-rebate.component';

describe('IncomeTaxRebateComponent', () => {
  let component: IncomeTaxRebateComponent;
  let fixture: ComponentFixture<IncomeTaxRebateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomeTaxRebateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeTaxRebateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
