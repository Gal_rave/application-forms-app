import { AfterViewInit, Component, Input, OnDestroy, ViewChild } from '@angular/core';
/*classes*/
import { EmployeeModel } from 'src/app/models/employee-model';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { Hmos } from 'src/app/enums/hmos.enum';
import { MaritalStates } from 'src/app/enums/marital-states.enum';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements AfterViewInit, OnDestroy {
  @ViewChild('EmployeeDetailsForm', { read: NgForm, static: false }) EmployeeDetailsForm: NgForm;

  private subscription: Subscription;
  public hmos = Hmos;
  public maritalStatesEnum = MaritalStates;
  public Resident: SelectBoolean[] = [
    { value: true, name: 'כן' },
    { value: false, name: 'לא' }
  ];
  public Genders: SelectBoolean[] = [
    { value: null, name: 'לא רלוונטי' },
    { value: true, name: 'זכר' },
    { value: false, name: 'נקבה' }
  ];

  @Input() employeeDetails: EmployeeModel;
  @Input() DisableAll: boolean = false;

  ngAfterViewInit() {
  }

  CheckFormValidation(): boolean {
    if (this.EmployeeDetailsForm === undefined)
      return false;
    if (this.EmployeeDetailsForm.invalid)
      Object.keys(this.EmployeeDetailsForm.controls).forEach(key => {
        this.EmployeeDetailsForm.controls[key].markAsDirty();
      });
    return this.EmployeeDetailsForm.invalid;
  }

  ngOnDestroy(){
    // this.subscription.unsubscribe();
  }
}
