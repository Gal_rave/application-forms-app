import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxFamilyDetailsComponent } from './tax-family-details.component';

describe('TaxFamilyDetailsComponent', () => {
  let component: TaxFamilyDetailsComponent;
  let fixture: ComponentFixture<TaxFamilyDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxFamilyDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxFamilyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
