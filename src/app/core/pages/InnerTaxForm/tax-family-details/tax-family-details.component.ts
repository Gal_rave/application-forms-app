import { Component, Input, ViewChild, ViewChildren, QueryList, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
/*classes*/
import { SpouseModel } from 'src/app/models/spouse-model';
import { SelectBoolean } from 'src/app/interfaces/select-boolean';
import { SelectString } from 'src/app/interfaces/select-string';
import { ChildModel } from 'src/app/models/child-model';
import { ChildrenComponent } from '../../LIstsComponents/children/children.component';

@Component({
  selector: 'tax-family-details',
  templateUrl: './tax-family-details.component.html',
  styleUrls: ['./tax-family-details.component.css']
})
export class TaxFamilyDetailsComponent implements OnInit {
  @ViewChild('TFD_Form', { read: NgForm, static: false }) TFD_Form: NgForm;

  @ViewChildren(ChildrenComponent) ChildrenViews: QueryList<ChildrenComponent>;

  public HasIncome: SelectBoolean[] = [
    { value: true, name: 'יש לבן/בת הזוג הכנסה' },
    { value: false, name: 'אין לבן/בת הזוג כל הכנסה' }
  ];
  public incomes: SelectString[] = [
    { value: '1', name: 'עבודה/קיצבה/עסק' },
    { value: '2', name: 'הכנסה אחרת' }
  ];

  @Input() Spouse: SpouseModel;
  @Input() Children: Array<ChildModel>;
  @Input() DisableAll: boolean = false;
  @Input() showSpouse: boolean = false;

  ngOnInit() {
    Object.keys(this.Spouse).forEach(key => {
      if (key.toLowerCase().includes('date') && this.Spouse[key] !== null && typeof this.Spouse[key] === 'string') {
        this.Spouse[key] = new Date(this.Spouse[key]);
        if (this.Spouse[key].getFullYear() < 1900)
          this.Spouse[key] = new Date();
      }
    });
  }

  CheckFormValidation(): boolean {
    if (this.TFD_Form === undefined)
      return false;

    /*validate all children*/
    if (this.ChildrenViews !== undefined && this.ChildrenViews.find(ic => ic.CheckFormValidation() === true) !== undefined)
      return true;

    if (this.TFD_Form.invalid)
      Object.keys(this.TFD_Form.controls).forEach(key => {
        this.TFD_Form.controls[key].markAsDirty();
      });

    return this.TFD_Form.invalid;
  }
}
