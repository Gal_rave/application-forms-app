import { Component, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
/*classes*/
import { TaxCoordinationModel } from 'src/app/models/tax-coordination-model';
import { SelectItem } from 'src/app/models/select-item';
import { TaxCoordinationStates } from 'src/app/enums/tax-coordination-states.enum';
import { TaxCoordinationEmployerComponent } from '../tax-coordination-employer/tax-coordination-employer.component';

@Component({
  selector: 'tax-coordination',
  templateUrl: './tax-coordination.component.html',
  styleUrls: ['./tax-coordination.component.css']
})
export class TaxCoordinationComponent {
  @ViewChild('TaxCoordinationForm', { read: NgForm, static: false }) TaxCoordinationForm: NgForm;
  @ViewChild(TaxCoordinationEmployerComponent) TCE_View: TaxCoordinationEmployerComponent;

  public immigrationStatus:SelectItem[] = [
    {Value:'עולה חדש/ה',Key:1},
    {Value:'תושב חוזר',Key:2}
  ];
  public tcStates = TaxCoordinationStates;
  
  @Input() Coordination: TaxCoordinationModel;
  @Input() DisableAll: boolean = false;

  CheckFormValidation(): boolean {
    if (this.TaxCoordinationForm === undefined)
      return false;
      if (this.TaxCoordinationForm.invalid)
      Object.keys(this.TaxCoordinationForm.controls).forEach(key => {
        this.TaxCoordinationForm.controls[key].markAsDirty();
      });
    return this.TaxCoordinationForm.invalid ||
    (this.TCE_View !== undefined && this.TCE_View.CheckFormValidation()) ;
  }
}
