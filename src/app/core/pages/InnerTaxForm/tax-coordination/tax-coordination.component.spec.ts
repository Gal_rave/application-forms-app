import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxCoordinationComponent } from './tax-coordination.component';

describe('TaxCoordinationComponent', () => {
  let component: TaxCoordinationComponent;
  let fixture: ComponentFixture<TaxCoordinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxCoordinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxCoordinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
