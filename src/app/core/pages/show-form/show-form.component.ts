import { Component, Input, OnInit, ChangeDetectionStrategy, ViewChild, Output, EventEmitter } from '@angular/core';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

/*component*/
import { WorkersUnionComponent } from '../workers-union/workers-union.component';
import { TravelReimbursementComponent } from '../travel-reimbursement/travel-reimbursement.component';
import { PersonalInformationComponent } from '../personal-information/personal-information.component';
import { TaxFormComponent } from '../tax-form/tax-form.component';
import { ProvidentFundComponent } from '../provident-fund/provident-fund.component';

/*classes*/
import { FormData } from 'src/app/models/form-data';
import { ShowForms } from 'src/app/models/show-forms';
import { MultiSelectSettings } from 'src/app/models/multi-select-settings';
import { MultiSelectTexts } from 'src/app/models/multi-select-texts';

/*enums*/
import { DeliveryStatusEnum } from 'src/app/enums/delivery-status.enum';

/*pipes*/
import { EnumToArrayPipe } from 'src/app/pipes/enum-to-array.pipe';

/*interfaces*/
import { EnumItem } from 'src/app/interfaces/enum-item';

/*services*/
import { FormsService } from '../../../services/forms-service';
import { ParentMassageService } from 'src/app/services/parent-massage.service';
import { NgForm } from '@angular/forms';
import { SnackBarCaller } from 'src/app/custom-material/snack-bar-caller';
import { KaTavla } from 'src/app/models/ka-tavla';
import { InformationTraffic } from 'src/app/models/information-traffic';
import { DataService } from 'src/app/services/data.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'show-form',
  templateUrl: './show-form.component.html',
  styleUrls: ['./show-form.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class ShowFormComponent implements OnInit {
  @ViewChild('ShowFormComponentForm', { read: NgForm, static: false }) ShowFormComponentForm: NgForm;

  @ViewChild(WorkersUnionComponent) WorkersUnionView: WorkersUnionComponent;
  @ViewChild(TravelReimbursementComponent) TravelReimbursementView: TravelReimbursementComponent;
  @ViewChild(PersonalInformationComponent) PersonalInformationView: PersonalInformationComponent;
  @ViewChild(TaxFormComponent) TaxFormView: TaxFormComponent;
  @ViewChild(ProvidentFundComponent) ProvidentFundView: ProvidentFundComponent;

  public OkStutas: Array<number> = [0, 2, 4];

  public multiSelectSettings: IMultiSelectSettings = new MultiSelectSettings();
  public multiSelectTexts: IMultiSelectTexts = new MultiSelectTexts();
  public deliveryStatusEnum = DeliveryStatusEnum;
  public closedDeliveryStatus = [{ index: -1, name: 'לא הגיש' }, { index: 5, name: 'סגור' }];
  public DeliveryStatuss: EnumItem[];

  @Input() form: FormData;

  /*Degrees=> 1096, KinshipTypes=> 1039 */
  @Input() KaData: Array<KaTavla>;
  @Input() ShowFactorys: boolean;
  @Input() ShowBudgetItems: boolean;

  public factory: IMultiSelectOption;
  public budgetItem: IMultiSelectOption;
  public isActive: boolean = false;
  public showForms: ShowForms;
  private Information: InformationTraffic;

  @Output() formOpenAction: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private formsService: FormsService, private postMassage: ParentMassageService, private barCaller: SnackBarCaller, private enumToArrayPipe: EnumToArrayPipe, private dataService: DataService) {
    this.DeliveryStatuss = enumToArrayPipe.transform(this.deliveryStatusEnum);
  }

  ngOnInit() {
    if (this.form.formsCounter > 0 && this.form.AppData !== null) {
      this.showForms = new ShowForms(this.form.AppData.MunicipalityForms, this.form.AppData);
      if (this.form.DELIVERYSTATUS === 5) {
        this.DeliveryStatuss = this.DeliveryStatuss.filter((i: EnumItem) => i.index < 0 || i.index === 5);
      }
      if (this.form.DELIVERYSTATUS === 4 && this.form.PERSONNELAPPROVAL > 0) {
        if (this.form.PERSONNELAPPROVAL === 2) {
          this.DeliveryStatuss = this.DeliveryStatuss.filter((i: EnumItem) => i.index === 4);
        } else {
          this.DeliveryStatuss = this.DeliveryStatuss.filter((i: EnumItem) => i.index > 3);
        }
      }
    } else {
      this.showForms = new ShowForms([], null);
      this.DeliveryStatuss = this.DeliveryStatuss.filter((i: EnumItem) => i.index < 0 || i.index === 5);
    }
    if (this.form.MIFAL !== null && this.form.MIFAL > 0) {
      this.factory = this.dataService.GetFactory(this.form.MIFAL);
    }
    if (this.form.SEIF_TAKZIVI_RASHI !== null && this.form.SEIF_TAKZIVI_RASHI > 0) {
      this.budgetItem = this.dataService.GetBudgetItem(this.form.SEIF_TAKZIVI_RASHI);
    }
  }
  CompleteDataAcquisition(): void {
    if (this.form.DELIVERYSTATUS !== 4 || this.form.PERSONNELAPPROVAL !== 1) {
      this.barCaller.PopMessage(false, 'לא ניתן לבצע עדכון בשנית', '');
      return;
    }
    this.Information = { MunicipalityId: this.form.MUNICIPALITYID, DocumentUserId: this.form.USERID.toString(), FormID: this.form.ID, EntryDate: this.form.CREATEDDATE, ImageTypes: this.form.AppData.MunicipalityForms, DocumentUserName: this.form.USERNAME, UserID: null, Response: null };

    this.formsService.SaveDocumentsToIfn(this.Information).subscribe((Information: InformationTraffic) => {
      if (Information.Response.ResponseCode !== 0) {/// an error has occurred
        this.barCaller.PopMessage(false, Information.Response.Message, 'התרחשה תקלה בשמירת המסמכים!');
        this.form.IFN_TRANSACTIONID = null;
        this.formsService.SetUplicationForm(this.form);
      } else {
        this.barCaller.PopMessage(true, 'תהליך קליטת הנתונים הסתיים.', 'המסמכים נשמרו לארכיון בהצלחה!');
        this.form.IFN_TRANSACTIONID = Information.Response.TransactionID;
        this.form.PERSONNELAPPROVAL = 2;
        this.formsService.SetUplicationForm(this.form);
      }
    },
      err => {
        this.barCaller.PopMessage(false, null, 'תקלת מערכת!');
        this.form.IFN_TRANSACTIONID = null;
        this.formsService.SetUplicationForm(this.form);
      });
  }
  HideCompleteDataAcquisition(): boolean {
    if (this.form.DELIVERYSTATUS !== 4 || this.form.PERSONNELAPPROVAL === 0) {
      return true;
    }
    return this.form.PERSONNELAPPROVAL !== 1;
  }
  HideDeliveryStatus(): boolean {
    if (this.form.DELIVERYSTATUS !== 4) {
      return true;
    }
    if (this.form.PERSONNELAPPROVAL > 0) {
      return true;
    }
  }
  ShowResetImport(): boolean {
    switch (this.form.DELIVERYSTATUS) {
      case 0:
      case 1:
      case 2:
      case 3:
        return false;
      default:
        return true;
    }
  }

  /*perform update on the form*/
  UpdateForm(): void {
    if (this.CheckFormValidation()) {
      this.barCaller.PopMessage(false, 'שגיאת נתונים!', 'שדות חובה אינם תקינים.');
      return;
    }
    if (this.form.DELIVERYSTATUS === 4) {
      this.formsService.Update(this.form).subscribe(frm => {
        this.form = frm;
        frm !== null && frm.ID > 0 ? this.CompleteDataAcquisition() : this.barCaller.PopMessage(false, 'התרחשה תקלה בשמירת הנתונים!');
      },
        err => {
          console.log('err', err);
        });
    }
  }
  ResetImport(): void {
    this.formsService.ReImportUserData(this.form).subscribe(frm => {
      this.barCaller.PopMessage(true, 'נתונים התעדכנו.');
    },
      err => {
        // console.error(err);
        this.barCaller.PopMessage(false, 'התרחשה תקלה בעידכון הנתונים!');
      });
  }
  /*check if all forms are valid*/
  CheckFormValidation(): boolean {
    if (this.ShowFormComponentForm === undefined) {
      return false;
    }

    if (this.ShowFormComponentForm.invalid) {
      Object.keys(this.ShowFormComponentForm.controls).forEach(key => {
        this.ShowFormComponentForm.controls[key].markAsDirty();
      });
    }

    return (this.WorkersUnionView !== undefined && this.WorkersUnionView.CheckFormValidation()) ||
      (this.TravelReimbursementView !== undefined && this.TravelReimbursementView.CheckFormValidation()) ||
      (this.PersonalInformationView !== undefined && this.PersonalInformationView.CheckFormValidation()) ||
      (this.TaxFormView !== undefined && this.TaxFormView.CheckFormValidation()) ||
      (this.ProvidentFundView !== undefined && this.ProvidentFundView.CheckFormValidation()) ||
      this.ShowFormComponentForm.invalid
      ;
  }
  /*open the forms row*/
  DisplayForms(event: any): void {
    this.isActive = !this.isActive;
    this.DisplayForm(-1);
  }
  /*open single form and close oll athers*/
  DisplayForm(formId: number) {
    if (this.CheckFormValidation()) {
      this.barCaller.PopMessage(false, 'שגיאת נתונים!', 'שדות חובה אינם תקינים.');
      return;
    }

    switch (formId) {
      case 1:
        this.showForms.PersonalInformation = !this.showForms.PersonalInformation;
        this.formOpenAction.emit(this.showForms.PersonalInformation);

        this.showForms.ProvidentFund = false;
        this.showForms.TravelReimbursement = false;
        this.showForms.WorkersUnion = false;
        this.showForms.TaxForm = false;
        this.showForms.ReferencesForm = false;
        this.showForms.RelativesForm = false;
        break;
      case 2:
        this.showForms.TravelReimbursement = !this.showForms.TravelReimbursement;
        this.formOpenAction.emit(this.showForms.TravelReimbursement);

        this.showForms.ProvidentFund = false;
        this.showForms.PersonalInformation = false;
        this.showForms.WorkersUnion = false;
        this.showForms.TaxForm = false;
        this.showForms.ReferencesForm = false;
        this.showForms.RelativesForm = false;
        break;
      case 3:
        this.showForms.WorkersUnion = !this.showForms.WorkersUnion;
        this.formOpenAction.emit(this.showForms.WorkersUnion);

        this.showForms.ProvidentFund = false;
        this.showForms.PersonalInformation = false;
        this.showForms.TravelReimbursement = false;
        this.showForms.TaxForm = false;
        this.showForms.ReferencesForm = false;
        this.showForms.RelativesForm = false;
        break;
      case 4:
        this.showForms.ProvidentFund = !this.showForms.ProvidentFund;
        this.formOpenAction.emit(this.showForms.ProvidentFund);

        this.showForms.WorkersUnion = false;
        this.showForms.PersonalInformation = false;
        this.showForms.TravelReimbursement = false;
        this.showForms.TaxForm = false;
        this.showForms.ReferencesForm = false;
        this.showForms.RelativesForm = false;
        break;
      case 6:
      case 7:
        this.showForms.ReferencesForm = !this.showForms.ReferencesForm;
        this.formOpenAction.emit(this.showForms.ReferencesForm);

        this.showForms.ProvidentFund = false;
        this.showForms.WorkersUnion = false;
        this.showForms.PersonalInformation = false;
        this.showForms.TravelReimbursement = false;
        this.showForms.TaxForm = false;
        this.showForms.RelativesForm = false;
        break;
      case 669:
        this.showForms.TaxForm = !this.showForms.TaxForm;
        this.formOpenAction.emit(this.showForms.TaxForm);

        this.showForms.ProvidentFund = false;
        this.showForms.PersonalInformation = false;
        this.showForms.TravelReimbursement = false;
        this.showForms.WorkersUnion = false;
        this.showForms.ReferencesForm = false;
        this.showForms.RelativesForm = false;
        break;
      default:
        this.formOpenAction.emit(false);
        this.showForms.ProvidentFund = false;
        this.showForms.PersonalInformation = false;
        this.showForms.TravelReimbursement = false;
        this.showForms.WorkersUnion = false;
        this.showForms.TaxForm = false;
        this.showForms.ReferencesForm = false;
        this.showForms.RelativesForm = false;
        break;
    }
  }

  ShowFormPdf(formId: number): void {
    this.postMassage.OpenPdfPopup('/portalsachar/Portal/ShowApplicantImage', this.form.MUNICIPALITYID, this.form.ID, formId);
  }

}
