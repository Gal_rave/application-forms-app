import { Component, Output, EventEmitter, Renderer2, ElementRef, OnInit, OnChanges, AfterContentInit, AfterViewInit, AfterViewChecked, Input } from '@angular/core';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';

/*classes*/
import { Filter } from 'src/app/models/filter';
import { SortModel } from 'src/app/models/sort-model';
import { MultiSelectSettings } from 'src/app/models/multi-select-settings';
import { MultiSelectTexts } from 'src/app/models/multi-select-texts';
import { ParentMassageService } from 'src/app/services/parent-massage.service';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  private interval: any;
  private intervalTime: number = 600;
  private down: string = 'fa-arrow-down';
  private up: string = 'fa-arrow-up';

  public hideUpdate: boolean = false;
  public hideExcel: boolean = false;

  public Factories: Observable<IMultiSelectOption[]>;
  public BudgetItems: Observable<IMultiSelectOption[]>;

  @Output("FilterListItems") OutputFilter = new EventEmitter<Filter>();
  @Output("SortListItems") SortingOutput = new EventEmitter<SortModel>();
  @Output("UpdateFormsStutas") UpdateStutas = new EventEmitter<boolean>();
  @Output("ExportExcel") ExportExcelEmitter = new EventEmitter<boolean>();

  @Input() counter: number;

  public multiSelectSettings: IMultiSelectSettings = new MultiSelectSettings({ id: 'showCheckAll', value: true }, { id: 'enableSearch', value: true }, { id: 'showUncheckAll', value: true });
  public multiSelectSettingsNoSearch: IMultiSelectSettings = new MultiSelectSettings();
  public multiSelectTexts: IMultiSelectTexts = new MultiSelectTexts();
  public filter: Filter = new Filter();

  public DeliveryStatuses: IMultiSelectOption[] = [{ id: 0, name: 'טופס הוגש' }, { id: 1, name: 'הוחזר לתיקון' }, { id: 2, name: 'הוגש שנית' },
  { id: 3, name: 'טופס נדחה' }, { id: 4, name: 'הושלם' }, { id: 5, name: 'טופס סגור' }, { id: -1, name: 'לא הגיש' }];

  constructor(private renderer: Renderer2, private elementRef: ElementRef, private massageService: ParentMassageService, public dataService: DataService) {
    this.interval = setInterval(() => '', this.intervalTime);
    clearInterval(this.interval);
  }

  ngAfterViewInit() {
    if (localStorage.getItem('OutputFilter') !== null) {
      setTimeout(() => {
        this.filter = JSON.parse(localStorage.getItem('OutputFilter'));
        this.OutputFilterAction(null);
      }, 1500);
    }
  }

  ngOnInit() {
    this.massageService.chatMessageAdded.subscribe((data) => {
      switch (data) {
        case 'externalExcelButton':
          this.hideExcel = true;
          break;
        case 'ExternalExcelClick':
          this.ExportExcel();
          break;
        case 'externalSaveButton':
          this.hideUpdate = true;
          break;
        case 'ExternalPFlikeClick':
          this.emitFormsUpdate();
          break;
        case 'GetRashutFactorys':
          this.Factories = this.dataService.CurrentFactoriesOptions;
          break;
        case 'GetBudgetItems':
          this.BudgetItems = this.dataService.CurrentBudgetItemsOptions;
          break;
      }
    });
  }

  ExportExcel(): void {
    this.ExportExcelEmitter.emit(true);
  }

  SaveFilter(): void {
    localStorage.setItem('OutputFilter', JSON.stringify(this.filter));
  }

  ClearFilter(): void {
    localStorage.removeItem('OutputFilter');
    this.filter = new Filter();
    this.OutputFilterAction(null);
  }

  OutputFilterAction(event: Event): void {
    clearInterval(this.interval);
    this.interval = setInterval(() => this.emitFilter(), this.intervalTime);
  }

  emitFilter(): void {
    this.OutputFilter.emit(this.filter);
    clearInterval(this.interval);
  }

  emitFormsUpdate(): void {
    this.UpdateStutas.emit(true);
  }

  OutputSortAction(event: any, sortItem: string): void {
    const hasClass = event.target.classList.contains(this.down);
    const dom: HTMLElement = this.elementRef.nativeElement;
    const elements = Array.from(dom.getElementsByClassName(this.up));
    elements.forEach(elm => {
      this.renderer.removeClass(elm, this.up);
      this.renderer.addClass(elm, this.down);
    });

    if (hasClass) {
      this.renderer.removeClass(event.target, this.down);
      this.renderer.addClass(event.target, this.up);
    } else {
      this.renderer.removeClass(event.target, this.up);
      this.renderer.addClass(event.target, this.down);
    }
    this.SortingOutput.emit({ SortItem: sortItem, SortDirection: hasClass });
  }
}
