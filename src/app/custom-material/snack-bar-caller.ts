import { OnInit, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSnackBarConfig } from '@angular/material/snack-bar';
import { ParentMassageService } from '../services/parent-massage.service';

@Injectable({
    providedIn: 'root'
})
export class SnackBarCaller {
    private snackBar_success: MatSnackBarConfig;
    private snackBar_error: MatSnackBarConfig;
    private hasframeElement: boolean = false;

    constructor(private snackBar: MatSnackBar, private massageService: ParentMassageService) {
        this.snackBar_success = new MatSnackBarConfig();
        this.snackBar_success.horizontalPosition = 'center';
        this.snackBar_success.verticalPosition = 'top';
        this.snackBar_success.panelClass = 'snach-bar-success';
        this.snackBar_success.duration = 2500;
        this.snackBar_success.politeness = 'assertive';
        this.snackBar_success.direction = 'rtl';

        this.snackBar_error = new MatSnackBarConfig();
        this.snackBar_error.horizontalPosition = 'center';
        this.snackBar_error.verticalPosition = 'top';
        this.snackBar_error.panelClass = 'snach-bar-error';
        this.snackBar_error.duration = 2500;
        this.snackBar_error.politeness = 'assertive';
        this.snackBar_error.direction = 'rtl';

        this.massageService.chatMessageAdded.subscribe((data) => {
            if (data === 'frameElement') {
                this.hasframeElement = true;
            }
        });
    }
    /*tslint:disable*/
    public PopMessage(msgType: boolean, Text: string, Title?: string) {
        switch (msgType) {
            case true:

                this.hasframeElement ? this.massageService.ParentMessage(this.MessageString(Title) ? `${Title}, ${Text}` : Text) :
                    this.snackBar.open(Text, this.MessageString(Title) ? Title : '', this.snackBar_success);
                break;
            case false:
                this.hasframeElement ? this.massageService.ParentMessage(this.MessageString(Title) ? `${Title}, ${Text}` : Text) :
                    this.snackBar.open(this.MessageString(Title) ? Title : '', Text, this.snackBar_error);
                break;
            default:
                this.hasframeElement ? this.massageService.ParentMessage(this.MessageString(Title) ? `${Title}, ${Text}` : Text) :
                    this.snackBar.open(Text, this.MessageString(Title) ? Title : '', this.snackBar_success);
                break;
        }
    }
    /*tslint:enaable*/
    private MessageString(str?: string): boolean {
        if (str !== null && typeof str === 'string' && str.length > 0) {
            return true;
        }
        return false;
    }
}
